module Main where

import Lib
import Evaluation.Performance (coreEnrichmentTest', coreForwardTest')
import Criterion.Main ( bench, nfIO, defaultMain,Benchmark ) 
import System.Directory (listDirectory)
import Data.List (isInfixOf)

adChangesData :: String 
adChangesData = "./data/ad/"

lidChangesData :: String
lidChangesData = "./data/lid/"

getTestList :: FilePath -> IO [FilePath]
getTestList dataPath =  do
    fs <- listDirectory dataPath
    let fs' = filter (\f -> not ("test" `isInfixOf` f)) fs 
    pure $ (dataPath <>)  <$> fs' 

main :: IO ()
main = do 
    files <- getTestList lidChangesData
    let
        enrichTestList = benchList "enrich" coreEnrichmentTest' <$> files 
        forwardTestList = benchList "forward" coreForwardTest' <$> files 
    defaultMain $ enrichTestList ++ forwardTestList
    where 
        benchList :: 
                String 
                -> (FilePath -> IO Double)
                -> FilePath 
                -> Benchmark 
        benchList label coreFunction filePath = 
            let 
                tag = label++":"++filePath
            in bench tag $ nfIO (coreFunction filePath)
