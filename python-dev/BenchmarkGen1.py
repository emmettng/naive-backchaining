from datetime import datetime

PROPOSITION_NAMES = []
GROUND_NAMES = []
RULE_NAMES = []

def proposition_name_generator():
    global PROPOSITION_NAMES
    if not len(PROPOSITION_NAMES):
        return_name = "q0"
        PROPOSITION_NAMES.append(return_name)
    else:
        tmp_name = PROPOSITION_NAMES[-1]
        number = int(tmp_name[1:])
        return_name = "q" + str(number+1)
        PROPOSITION_NAMES.append(return_name)
    return return_name

def rule_name_generator():
    global RULE_NAMES
    if not len(RULE_NAMES):
        return_name = "r0"
        RULE_NAMES.append(return_name)
    else:
        tmp_name = RULE_NAMES[-1]
        number = int(tmp_name[1:])
        return_name = "r" + str(number+1)
        RULE_NAMES.append(return_name)
    return return_name


def top_proposition_generator(initial_name, top_proposition_num):
    top_proposition_list = []
    tmp_name = initial_name
    for _ in range(top_proposition_num):
        tmp_name = proposition_name_generator(tmp_name)
        top_proposition_list.append(tmp_name)
    return top_proposition_list

def defeater_generator(defender, total_depth, defeasible_depth):
    return

def unwarranted_generator(top_warranted_list,split_factor,total_depth, defeasible_depth):
    return

def warranted_generator(top_warranted_list,split_factor,total_depth, defeasible_depth):
    top_enrichment_list = []
    for p in top_warranted_list:
        top_enrichment_list.append(enrichment_generator(p,split_factor,total_depth,defeasible_depth))

'''
Auxiliary functions
'''
def select_defeasible_proposition(enrichment_rules):
    defeasible_proposition_list = []
    for r in enrichment_rules:
        if "=>" not in r : continue
        Conc = r.split('>')[-1]
        if Conc in defeasible_proposition_list : continue
        defeasible_proposition_list.append(Conc)
    return defeasible_proposition_list

def flatten(list_of_lists):
    set = []
    for l in list_of_lists:
        for e in l:
            if e in set : continue
            set.append(e)
    return set

def negate_proposition_generator(proposition):
    if '!' in proposition[0]:
        return proposition[1:]
    else:
        return "!"+proposition

def winner_generator_eli(target_enrichment,winner_type):
    new_rule_collection = []
    if winner_type == "total":
        value = "99"
    elif winner_type == "half":
        value = "50"
    else:
        value = "1"

    for r in target_enrichment:
        new_rule_collection.append(r+","+value)
    return new_rule_collection

def loser_generator_eli(target_enrichment):
    new_rule_collection = []
    for r in target_enrichment:
        new_rule_collection.append(r+",1")
    return new_rule_collection

def branch(target_enrichment,split_factor, total_depth,defeasible_depth):
    defeasible_propositions = select_defeasible_proposition(target_enrichment)
    branch = [enrichment_generator(p,split_factor,total_depth - defeasible_depth,0) for p in defeasible_propositions]
    return flatten(branch)

def save_to_file(target_enrichment,name):
    with open(name+".txt",'w') as f :
        for rule in target_enrichment:
            f.write('%s\n' % rule)
'''
All arguments are Self-Warranted : no attacker
'''
def type_1_generator(proposition, split_factor,total_depth, defeasible_depth):
    target_enrichment = enrichment_generator(proposition, split_factor,total_depth, defeasible_depth)
    return target_enrichment

'''
All arguments are Self-Warranted : rebut detected on all arguments (failure attacker)
    failure: using Eli, so all defeasible rules in target_enrichment is more preference than arguments in enrichments of failure defeaters.
'''
def type_2_generator(proposition, split_factor,total_depth, defeasible_depth):
    target_enrichment = enrichment_generator(proposition, split_factor,total_depth, defeasible_depth)
    defeasible_propositions = select_defeasible_proposition(target_enrichment)
    negation_propositions = [negate_proposition_generator(dp) for dp in defeasible_propositions]
    tmp_rebut_enrichments = [enrichment_generator(np, split_factor,total_depth,defeasible_depth)  for np in negation_propositions]
    target_enrichment_pref = winner_generator_eli(target_enrichment,"total")
    rebut_enrichments_pref = [loser_generator_eli(enrichment) for enrichment in tmp_rebut_enrichments]
    # return target_enrichment_pref , rebut_enrichments_pref
    return target_enrichment_pref + flatten(rebut_enrichments_pref)

'''
All arguments are Warranted : undercut defeater on all arguments.
    enrichment of undercut proposition are defeated by type 1 .
'''
def type_3_a_generator(proposition, split_factor,total_depth, defeasible_depth):
    return

'''
All arguments are Warranted : undercut defeater on all arguments.
    enrichment of undercut proposition are defeated by type 2. 
'''
def type_3_b_generator(proposition, split_factor,total_depth, defeasible_depth):
    return

'''
All arguments are Warranted : rebut defeater on all arguments.
    enrichment of undercut proposition are defeated by type 1 .
'''
def type_4_a_generator(proposition, split_factor,total_depth, defeasible_depth):
    target_enrichment = enrichment_generator(proposition, split_factor,total_depth, defeasible_depth)

    defeasible_propositions = select_defeasible_proposition(target_enrichment)
    negation_propositions = [negate_proposition_generator(dp) for dp in defeasible_propositions]
    tmp_rebut_enrichments = [enrichment_generator(np, split_factor,total_depth,defeasible_depth)  for np in negation_propositions]

    target_enrichment_pref = loser_generator_eli(target_enrichment)
    rebut_enrichments_pref = [winner_generator_eli(enrichment,"half") for enrichment in tmp_rebut_enrichments]

    lower_level_defeasible_proposition = flatten ([select_defeasible_proposition(enr) for enr in tmp_rebut_enrichments])
    neg_lower_level_defeasible_proposition = [negate_proposition_generator(dp) for dp in lower_level_defeasible_proposition]
    defenders = [type_1_generator(p,split_factor,total_depth,defeasible_depth) for p in neg_lower_level_defeasible_proposition]
    lower_levels_rules = flatten(defenders)

    # return target_enrichment_pref, rebut_enrichments_pref, defenders
    return target_enrichment_pref, flatten(rebut_enrichments_pref), lower_levels_rules

'''
All arguments are Warranted : rebut defeater on all arguments.
    enrichment of undercut proposition are defeated by type 2. 
'''
def type_4_b_generator(proposition, split_factor,total_depth, defeasible_depth):
    target_enrichment = enrichment_generator(proposition, split_factor,total_depth, defeasible_depth)

    defeasible_propositions = select_defeasible_proposition(target_enrichment)
    negation_propositions = [negate_proposition_generator(dp) for dp in defeasible_propositions]
    tmp_rebut_enrichments = [enrichment_generator(np, split_factor,total_depth,defeasible_depth)  for np in negation_propositions]

    target_enrichment_pref = loser_generator_eli(target_enrichment)
    rebut_enrichments_pref = [winner_generator_eli(enrichment,"half") for enrichment in tmp_rebut_enrichments]

    lower_level_defeasible_proposition = flatten ([select_defeasible_proposition(enr) for enr in tmp_rebut_enrichments])
    neg_lower_level_defeasible_proposition = [negate_proposition_generator(dp) for dp in lower_level_defeasible_proposition]
    defenders = [type_2_generator(p,split_factor,total_depth,defeasible_depth) for p in neg_lower_level_defeasible_proposition]
    lower_levels_rules = flatten(defenders)

    # return target_enrichment_pref, rebut_enrichments_pref, defenders
    return target_enrichment_pref, flatten(rebut_enrichments_pref), lower_levels_rules

'''
Half arguments are type 1 self warranted,
Half argument are type 3 warranted.
'''
def type_5_a_generator(proposition, split_factor,total_depth, defeasible_depth):
    return



'''
Half arguments are type 1 self warranted,
Half argument are type 4.b warranted.
'''
def type_5_b_generator(proposition, split_factor,total_depth, defeasible_depth):
    target_enrichment = enrichment_generator(proposition, split_factor,total_depth, defeasible_depth)

    half_1_enrichment = target_enrichment
    half_1_enrichment_pref= winner_generator_eli(half_1_enrichment,"total")

    half_2_enrichment = branch(target_enrichment,split_factor,total_depth, defeasible_depth)

    defeasible_propositions = select_defeasible_proposition(half_2_enrichment)
    negation_propositions = [negate_proposition_generator(dp) for dp in defeasible_propositions]
    tmp_rebut_enrichments = [enrichment_generator(np, split_factor,total_depth,defeasible_depth)  for np in negation_propositions]

    half_2_enrichment_pref = loser_generator_eli(half_2_enrichment)
    rebut_defeater_enrichment_pref = [winner_generator_eli(enrichment,"half") for enrichment in tmp_rebut_enrichments]

    lower_level_defeasible_proposition = flatten ([select_defeasible_proposition(enr) for enr in tmp_rebut_enrichments])
    neg_lower_level_defeasible_proposition = [negate_proposition_generator(dp) for dp in lower_level_defeasible_proposition]
    defenders = [type_2_generator(p,split_factor,total_depth,defeasible_depth) for p in neg_lower_level_defeasible_proposition]
    lower_levels_rules = flatten(defenders)


    return half_1_enrichment_pref, half_2_enrichment_pref, rebut_defeater_enrichment_pref, defenders

'''
Half arguments are type 2 self warranted,
Half argument are type 3 warranted.
'''
def type_5_c_generator(proposition, split_factor,total_depth, defeasible_depth):
    return

'''
Half arguments are type 2 self warranted,
Half argument are type 4 warranted.
'''
def type_5_d_generator(proposition, split_factor,total_depth, defeasible_depth):
    return

def enrichment_generator(proposition, split_factor,total_depth, defeasible_depth):
    to_be_support_conc_list = [proposition]
    rule_collections = []
    for i in range(total_depth):
        if i == defeasible_depth - 1 :
            rule_type = "=>"
        else:
            rule_type = "->"
        tmp_proposition_list = []
        for tmp_head in to_be_support_conc_list:

            tmp_rule_list = []
            for r in range(split_factor):  ## each conc supported by 'split_factor' rules

                if i == total_depth-1:
                    ground_rule = ground_generator(tmp_head,rule_type)
                    tmp_rule_list += ground_rule
                else:
                    tmp_body = []
                    for b in range(split_factor):  ## each rule contains 'split_factor' number of body propositions
                        tmp_pro_name = proposition_name_generator()
                        tmp_body.append(tmp_pro_name)  ## get the rule body
                    tmp_proposition_list = tmp_proposition_list + tmp_body  ## record to_do_conc_list at this level
                    tmp_rule_list.append(rule_generator(tmp_head, tmp_body,rule_type))  ## get all rule related to this tmp_head

            rule_collections = rule_collections + tmp_rule_list  ## put in the rule_collections

        to_be_support_conc_list = tmp_proposition_list  ## update to_do_conc_list and start again.

    enrichment_rules = rule_collections

    return enrichment_rules


def ground_generator(head,rule_type):
    global GROUND_NAMES
    if head in GROUND_NAMES: return []
    rule_name = rule_name_generator()
    return_rule = rule_name+ ":"+ rule_type + head
    GROUND_NAMES.append(head)

    return [return_rule]

def rule_generator(head,body,rule_type):
    rule_name = rule_name_generator()
    return_rule = rule_name+":"
    for b in body:
        return_rule = return_rule + str(b) +","
    if len(body):
        return_rule = return_rule[:-1] + rule_type + head
    else:
        return_rule = return_rule + rule_type + head

    return return_rule


def batch_generator(proposition_num, total_depth_range, split_factor, defeasible_depth_range, relational_tree_type, dirc = "./test-data/"):
    top_proposition_list = []
    print ("Number of top proposition is: %d" % proposition_num)
    print ("Generating top proposiiton...")

    for i in range(proposition_num):
        tmp_proposition_name = proposition_name_generator()
        top_proposition_list.append(tmp_proposition_name)

    type_name = relational_tree_type.__name__
    print ("%d top proposition initialization finished !!!" % len(top_proposition_list))
    print ("Start generating knowledge base with given type: %s" % type_name)

    for total_depth in total_depth_range:
        for defeasible_depth in defeasible_depth_range:
            if defeasible_depth > total_depth : continue
            startT = datetime.now()
            print ("generating rules with total depth %d and defeasible depth depth %d" % (total_depth, defeasible_depth))
            file_path= dirc + type_name+ "_topnum:"+ str(proposition_num)+"_" + "td:" + str(total_depth) + "_" + "dd:" + str(defeasible_depth)
            rules = single_generator(top_proposition_list,total_depth,split_factor,defeasible_depth,relational_tree_type)
            print ("saving %d rules to file %s" % (len(rules), file_path))
            endT = datetime.now()
            print ("start at %s , finish at %s, use %s" % (str(startT), str(endT),str(endT-startT)))
            print ("")
            save_to_file(rules,file_path)
            reset_GLOBAL()

def single_generator(proposition_list, total_depth, split_factor,defeasible_depth,relational_tree_type):

    total_rules = []
    acc = 0
    for proposition in proposition_list:
        tmp_rules = relational_tree_type(proposition,split_factor,total_depth,defeasible_depth)
        for l in tmp_rules:
            total_rules +=l
        acc += 1
        print ("%d top propositions has finished " % acc)

        # prec = 100*(acc / len(proposition_list))
        # if prec % 20 == 0:
        #     print (" %d precent top propositions has finished " % prec)

    return total_rules

def reset_GLOBAL():
    global PROPOSITION_NAMES
    global RULE_NAMES
    global GROUND_NAMES
    PROPOSITION_NAMES = []
    RULE_NAMES = []
    GROUND_NAMES = []z
    return
'''
Auxilary functions being used when programming , not part of the program.
'''
def count(split_factor,depth):
    acc = 0
    for i in range(1,depth+1):
        tmp = split_factor*split_factor**(2*(i-1))
        acc += tmp
        # print (acc)
    amun = split_factor*split_factor**(2*(depth-1))

    return (amun,acc)

if __name__== "__main__":
    #
    # split_factor = 2
    # total_depth = 3
    #
    # defeasible_depth = min(1, total_depth)

    batch_generator(1,[3,4,5,6],2,[3,4,5,6],type_4_b_generator)

    '''
    test branch function
    '''
    # test_half_half= enrichment_generator("t",split_factor,total_depth, defeasible_depth)
    # print (test_half_half)
    # branch = branch(test_half_half, split_factor,total_depth, defeasible_depth)
    # print (branch)
    # print (len(branch))

    '''
    test enrichment_genreator
    '''
    # rl = enrichment_generator("t",split_factor,total_depth, defeasible_depth)
    # print (rl)
    # print (len(rl))
    # print(count(split_factor,total_depth))
    #
    # r1 = rl[0]
    # print (r1)
    # rt = "rt:q1,q2,q3=>c123"
    # # conc = r1.split('>')
    # conc = rt.split('>')
    # print (conc[-1])
    # print (rt)

    '''
    test type 1 data set generator
    '''
    # type_1_rules = type_1_generator("t0",split_factor,total_depth,defeasible_depth)
    # print (type_1_rules)
    # print (len(type_1_rules))
    # print ("")

    '''
    test type 2 data set generator
    '''
    # type_2_rules = type_2_generator("t0",split_factor,total_depth,defeasible_depth)
    # print (type_2_rules)
    # print (len(type_2_rules))
    #
    # print (type_2_rules[0])
    # print (len(type_2_rules[0]))

    # for defeater in type_2_rules[1]:
    #    print (defeater)

    '''
    test type 4_a data set generator
    '''
    # type_4_a_rules = type_4_a_generator("t0",split_factor,total_depth,defeasible_depth)
    # for l in type_4_a_rules:
    #     print (l)
    #     print (len(l))

    '''
    test type 4_b data set generator
    '''
    # type_4_b_rules = type_4_b_generator("q",split_factor,total_depth,defeasible_depth)
    # for l in type_4_b_rules:
    #     print (l)
    #     print (len(l))

    '''
    test type 5_b data set generator
    '''
    # type_5_b_rules = type_5_b_generator("t0",split_factor,total_depth,defeasible_depth)
    # for l in type_5_b_rules:
    #     print (l)
    #     print (len(l))

    '''
    test writing to file 
    '''
    # type_4_b_rules = type_4_b_generator("q",split_factor,total_depth,defeasible_depth)
    # acc = []
    # for l in type_4_b_rules:
    #     acc +=l
    # print (acc)
    # save_to_file(acc,"test_type_4_b")
    # #
    # print (len(acc))

    '''
    test bech generator 
        top proposition, split_factor , total_depth ,   rules,          size
        500                 2           2               425000,     9.8     MB
        5000                2           2               4250000,    107.9   MB
        500                 2           3               1785000,    45.1    MB
    '''
    # target_top_proposition_num = 5000
    # top_proposition_list = []
    # for i in range(target_top_proposition_num):
    #     tmp_proposition_name = proposition_name_generator()
    #     top_proposition_list.append(tmp_proposition_name)
    # # print (top_proposition_list)
    # print (len(top_proposition_list))
    #
    # total_rules = []
    # for p in top_proposition_list:
    #     tmp_rules = type_4_b_generator(p,split_factor,total_depth,defeasible_depth)
    #     for l in tmp_rules:
    #         total_rules += l
    # print (len(total_rules))
    # save_to_file(total_rules,"type_4_b_"+str(target_top_proposition_num)+"_top_"+str(split_factor)+"_sp"+str(total_depth)+"_td")

'''
              depth 
split factor    1           2           3               4               5  
        1       1           2           3               4               5
        2       2/2         8/10        32/42       128/170         512/682
                4           16          64          256             1024
        3       3/3         27/30       243/273     2187/2460       19683/22143
                9           81          729         6561            59049
        4       4/4         64/68       1024/1092   16384/17476     262144/279620
        
        5       5/5         125/130     3125/3255   78125/81380     1953125/2034505
      
      split_factor*(split_factor)^2*(depth-1)
        
\sum_1^5 split_factor^{depth}
'''
    # for i in range(1,10):
    #     name = proposition_name_generator()
    #     print (name)
    # print (PROPOSITION_NAMES)
    #
    # for i in range(0,10):
    #     name = rule_name_generator()
    #     print (name)
    # print (RULE_NAMES)
    # rpa = [1,10]
    # rs = [1,10]    #
    # d = [.2,.5]    # depth of last defeasible rules.
    # max = []     # depth of top conclusion


'''
top warranted / unwarranted 
level 1 warranted / unwarranted 
'''
    # total_c = 10
    # number of enrichment of the given proposition
    # attack_i = .4
    # print (list(split_factor))

    # tmpL= top_proposition_generator("q0",10)
    # print (tmpL)

    # print (list(map((lambda x : round(10*x)), (list(defeasible_depth_list)))))
'''
construct 10000 first 
according to flip times 
'''
# half warranted
# hale unwarranted

'''
3. only the lastest unwarranted defeater got recorded. 
'''


'''
1。 lucky set : 当可能存在selfwarranted 时，发现（potential） defeater 的 argument 转移之waiting set 中， 不做处理。 
    多于一个可能的self warranted argument 选哪一个呢？ 
2。 waiting set: 
    多于一个发现了 （potential ）defeater 的 argument， 哪一个 （potential defeater） 应该首先被query， 
    需要找多最有可能时unwarranted 那一个，这样就可以把相关的 argument 转移会 lucky set中， 并继续之前一个 层级的 构建  
'''