import requests
import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
from matplotlib import cm

## File that contains test data
# file = "/Users/whsdu/workspace/aberdeen/BCOptimization/demoExamples/search/test-bruno.txt"
# file = "/Users/whsdu/workspace/aberdeen/BCOptimization/demoExamples/search/demo.txt"
file = "test-data/c-test5.txt"

## query proposition
query = "p1"

URL = "http://localhost:8081/"
# URL = "http://18.170.254.62:8081/"

def testEnv(filePath, query):
	global URL
	f = open(filePath,'rb')
	# print (f)
	files = {"file" : f}
	data = {"query" : query}
	res = requests.post(URL,files=files, data=data)
	return res

def parseFileNames (files, targettype):
	td_list = []
	dd_list = []
	cn_list = []

	for f in files :
		f = f[0:-4]
		[type, tds,dds,cns] = f.split('-')
		if type != targettype : continue
		# print (f)
		cnn = cns.split(':')[-1]
		tdn = tds.split(':')[-1]
		ddn = dds.split(':')[-1]

		if cnn not in cn_list:
			cn_list.append(cnn)
		if tdn not in td_list :
			td_list.append(tdn)
		if ddn not in dd_list:
			dd_list.append(ddn)

	fileArray = np.empty((len(cn_list),len(td_list),len(dd_list)))

	print (td_list, dd_list,cn_list)
	td_list.sort(key=int)
	dd_list.sort(key=int)
	cn_list.sort(key=int)
	print (td_list, dd_list,cn_list)

	for f in files :
		freq = f
		f = f[0:-4]
		[type, tds,dds,cns] = f.split('-')
		if type != targettype : continue
		# print (f)
		tdn = tds.split(':')[-1]
		ddn = dds.split(':')[-1]
		cnn = cns.split(':')[-1]

		freq = "test-data/" + freq
		print (freq)
		res = testEnv(freq,"q0")
		lll = float(str(res.content).split('<')[-1].split(" ")[1])
		print (lll)
		print ("")
		fileArray[cn_list.index(cnn),td_list.index(tdn),dd_list.index(ddn)] = lll

	return (cn_list,td_list,dd_list,fileArray)

def get_evalu_data():
	type_list = ["type_0_generator","type_1_generator","type_2_generator","type_3_generator"]
	dir = "test-data/"
	files = os.listdir(dir)
	for test_type in type_list:
		(cn_list,td_list,dd_list,fileArray) = parseFileNames(files,test_type)
		with open("eval-data/"+test_type+".pickle", 'wb') as handle:
			pickle.dump((cn_list,td_list,dd_list,fileArray), handle, protocol=pickle.HIGHEST_PROTOCOL)

def eval_data():
	dir = "eval-data/"
	files = os.listdir(dir)
	print (files)
	# total_depths = [10,15,20,25,30]
    # def_depth_factors = [0.2,0.4,0.6,0.8]
    # # confusion_num_list = [1,5,10]
    # confusion_num_list = [1,5]
    # top_prop_num = 10
	for f in files[3:]:
		path = dir + f
		with open(path,'rb') as handle:
			(cn_list,td_list,dd_list,fileArray) = pickle.load(handle)
			print (path)
			print ((cn_list,td_list,dd_list,fileArray))
			print ("")

		draw0 = fileArray[0,:,:]
		draw1 = fileArray[1,:,:]
		x = list(map(lambda x : int(x), dd_list))
		y = list(map(lambda x : int(x), td_list))
		X, Y = np.meshgrid(x, y)
		# print(draw.shape)
		print (X.shape)
		print (Y.shape)
		# X, Y = np.meshgrid(total_depth_list, defeasible_depth_list)
		# Z_1 = np.reshape(pref_1, X.shape)
		# Z_2 = np.reshape(pref_2, X.shape)
		fig = plt.figure()
		ax = fig.gca(projection='3d')

		# ax.plot_surface(X, Y, Z_1,cmap='viridis',linewidth=0)
		# ax.plot_surface(X, Y, Z_2,cmap='viridis',linewidth=0)

		# ax.scatter(X, Y, Z_1,cmap=cm.coolwarm ,linewidth=0)
		# ax.scatter(X, Y, Z_2,cmap=cm.coolwarm ,linewidth=0)

		# ax.plot_surface(X, Y, draw0, cmap='viridis', linewidth=0)
		# ax.plot_surface(X, Y, draw1, cmap='viridis', linewidth=0)

		# ax.plot_surface(X, Y, draw0, cmap=cm.coolwarm, linewidth=0)
		ax.plot_surface(X, Y, draw1, cmap=cm.coolwarm, linewidth=0)

		ax.set_ylabel('total_depth')
		ax.set_xlabel('defeasible_depth')
		ax.set_zlabel('preformance')
		plt.show()
		break

if __name__ == "__main__":
	# get_evalu_data()
	eval_data()