import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.stats import multivariate_normal
from mpl_toolkits.mplot3d import Axes3D



total_depth_list = [1,2,3,4,5]
defeasible_depth_list = [1,2,3,4,5]
pref_1 = [11,12,13,14,15,
          21,22,23,24,25,
          31,32,33,34,35,
          41,42,43,44,45,
          31,32,33,34,35]

pref_2 = [21,22,23,24,25,
          31,32,33,34,35,
          41,42,43,44,45,
          51,52,53,54,55,
          41,42,43,44,45]

X, Y = np.meshgrid(total_depth_list,defeasible_depth_list)
Z_1 = np.reshape(pref_1,X.shape)
Z_2 = np.reshape(pref_2,X.shape)
fig = plt.figure()
ax = fig.gca(projection='3d')


# ax.plot_surface(X, Y, Z_1,cmap='viridis',linewidth=0)
# ax.plot_surface(X, Y, Z_2,cmap='viridis',linewidth=0)

# ax.scatter(X, Y, Z_1,cmap=cm.coolwarm ,linewidth=0)
# ax.scatter(X, Y, Z_2,cmap=cm.coolwarm ,linewidth=0)

print (X)
print (Y)
print (Z_1)

print (X.shape)
print (Y.shape)
print (Z_1.shape)

ax.plot_surface(X, Y, Z_1,cmap=cm.coolwarm ,linewidth=0)
ax.plot_surface(X, Y, Z_2,cmap=cm.coolwarm ,linewidth=0)

ax.set_xlabel('total_depth')
ax.set_ylabel('defeasible_depth')
ax.set_zlabel('preformance')
plt.show()

'''
1。批量产生数据集
    total-depth-list X defeasible-depth-list , each 500 
2. 修改naive-backchaining
3。run bath jobs
4. each job generate a graph. 
5. 当defeasible depth 为 1 时， 很有意思，TODO
'''
