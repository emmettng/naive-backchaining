import requests


## File that contains test data
# file = "/Users/whsdu/workspace/aberdeen/BCOptimization/demoExamples/search/test-bruno.txt"
# file = "/Users/whsdu/workspace/aberdeen/BCOptimization/demoExamples/search/demo.txt"
file = "test-data/c-test5.txt"

## query proposition
query = "p1"

URL = "http://localhost:8081/"
# URL = "http://18.170.254.62:8081/"

def testEnv(filePath, query):
	global URL
	f = open(filePath,'rb')
	print (f)
	files = {"file" : f}
	data = {"query" : query}
	res = requests.post(URL,files=files, data=data)
	return res

if __name__ == "__main__":
	r = testEnv(file,query)
	print(r.content)
