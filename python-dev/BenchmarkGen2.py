from datetime import datetime

PROPOSITION_NAMES = []
GROUND_NAMES = []
RULE_NAMES = []

def proposition_name_generator():
    global PROPOSITION_NAMES
    if not len(PROPOSITION_NAMES):
        return_name = "q0"
        PROPOSITION_NAMES.append(return_name)
    else:
        tmp_name = PROPOSITION_NAMES[-1]
        number = int(tmp_name[1:])
        return_name = "q" + str(number+1)
        PROPOSITION_NAMES.append(return_name)
    return return_name

def rule_name_generator():
    global RULE_NAMES
    if not len(RULE_NAMES):
        return_name = "r0"
        RULE_NAMES.append(return_name)
    else:
        tmp_name = RULE_NAMES[-1]
        number = int(tmp_name[1:])
        return_name = "r" + str(number+1)
        RULE_NAMES.append(return_name)
    return return_name

def rule_generator(head,body,rule_type):
    rule_name = rule_name_generator()
    return_rule = rule_name+":"
    for b in body:
        return_rule = return_rule + str(b) +","
    if len(body):
        return_rule = return_rule[:-1] + rule_type + head
    else:
        return_rule = return_rule + rule_type + head

    return return_rule

def negate_proposition_generator(proposition):
    if '!' in proposition[0]:
        return proposition[1:]
    else:
        return "!"+proposition


def path_generator(proposition,total_depth,def_depth):
    head = proposition
    path = []
    def_pro = ""
    def_rule = ""
    for i in range(total_depth):
        ruleName = rule_name_generator()
        body = proposition_name_generator()

        if i == def_depth - 1:
            rule_type = "=>"
            def_pro = head
            def_rule = ruleName
        else:
            rule_type = "->"

        if i == total_depth -1 :
            rule = ruleName + ":" + rule_type + head
        else:
            rule = ruleName + ":" + body + rule_type + head
        head = body
        path.append(rule)
    return path, def_pro, def_rule

def assignPreference (path, type=0):
    newPath = []
    for p in path:
        newPath.append(p +"," + str(type))
    return newPath

def unwarranted_path_generator(proposition,total_depth,def_depth):
    path , def_pro, def_rule = path_generator(proposition, total_depth, def_depth)
    undercut_pont = negate_proposition_generator(def_rule)
    defeater , _, _ = path_generator(undercut_pont,total_depth,def_depth)

    path = assignPreference(path)
    defeater = assignPreference(defeater)
    return path, defeater

def save_to_file(rules,name):
    with open(name+".txt",'w') as f :
        for rule in rules:
            f.write('%s\n' % rule)

def type_0_generator(proposition,total_depth,def_depth,confusion_num):
    path , def_pro, def_rule = path_generator(proposition, total_depth, def_depth)
    rebut_point = negate_proposition_generator(def_pro)
    unWdefeater, _, _ = path_generator(rebut_point,total_depth,def_depth)

    path = assignPreference(path, 99)
    unWdefeater = assignPreference(unWdefeater,0)

    confusion_list = []
    for i in range(confusion_num):
        cPath, cDefeater = unwarranted_path_generator(proposition,total_depth,def_depth)

        # confusion_list.append((cPath, cDefeater))       ## for debugging
    # return path, unWdefeater, confusion_list            ## for debugging
        confusion_list += cPath + cDefeater             ## for production use
    rules = path + unWdefeater + confusion_list     ## for production use
    return rules                                        ## for production use


def type_1_generator(proposition,total_depth,def_depth,confusion_num):
    path , def_pro_1, _ = path_generator(proposition, total_depth, def_depth)
    rebut_point_1 = negate_proposition_generator(def_pro_1)

    defeater, def_pro_2 , _ = path_generator(rebut_point_1,total_depth,def_depth)
    rebut_point_2 = negate_proposition_generator(def_pro_2)

    defeater2, _, _ = path_generator(rebut_point_2,total_depth,def_depth)

    path = assignPreference(path,0)
    defeater = assignPreference(defeater,2)
    defeater2 = assignPreference(defeater2,3)

    confusion_list = []
    for i in range(confusion_num):
        cPath, cDefeater = unwarranted_path_generator(proposition,total_depth,def_depth)
        # confusion_list.append((cPath, cDefeater))     ## for debugging
        confusion_list += cPath + cDefeater           ## for production use

    # return path , defeater, defeater2, confusion_list
    rules = path + defeater + defeater2 + confusion_list
    return rules

def type_2_generator(proposition,total_depth,def_depth,confusion_num):
    path , _ , def_rule_1 = path_generator(proposition, total_depth, def_depth)
    undercut_point = negate_proposition_generator(def_rule_1)

    defeater, def_pro_2 , _ = path_generator(undercut_point,total_depth,def_depth)
    rebut_point_2 = negate_proposition_generator(def_pro_2)

    defeater2, _, _ = path_generator(rebut_point_2,total_depth,def_depth)

    path = assignPreference(path,0)
    defeater = assignPreference(defeater,2)
    defeater2 = assignPreference(defeater2,3)

    confusion_list = []
    for i in range(confusion_num):
        cPath, cDefeater = unwarranted_path_generator(proposition,total_depth,def_depth)
        # confusion_list.append((cPath, cDefeater))     ## for debugging
        confusion_list += cPath + cDefeater       ## for production use
    # return path , defeater, defeater2, confusion_list
    rules = path + defeater + defeater2 + confusion_list
    return rules


def type_3_generator(proposition,total_depth,def_depth,confusion_num):
    path , def_pro_1 , def_rule_1 = path_generator(proposition, total_depth, def_depth)
    undercut_point = negate_proposition_generator(def_rule_1)
    rebut_point_1 = negate_proposition_generator(def_pro_1)

    undercutter , def_pro_2 , _ = path_generator(undercut_point,total_depth,def_depth)
    rebut_point_undercutter = negate_proposition_generator(def_pro_2)
    defeater2, _, _ = path_generator(rebut_point_undercutter,total_depth,def_depth)

    rebutter, def_pro_3 , _ = path_generator(rebut_point_1,total_depth,def_depth)
    rebut_point_rebutter = negate_proposition_generator(def_pro_3)
    defeater3, _, _ = path_generator(rebut_point_rebutter,total_depth,def_depth)

    path = assignPreference(path,0)

    undercutter = assignPreference(undercutter,2)
    defeater2 = assignPreference(defeater2,3)

    rebutter = assignPreference(rebutter,2)
    defeater3 = assignPreference(defeater3,3)


    confusion_list = []
    for i in range(confusion_num):
        cPath, cDefeater = unwarranted_path_generator(proposition,total_depth,def_depth)
        # confusion_list.append((cPath, cDefeater))     ## for debugging
        confusion_list += cPath + cDefeater       ## for production use
    # return path , undercutter, defeater2, rebutter, defeater3, confusion_list
    rules = path + undercutter + defeater2 + rebutter + defeater3 + confusion_list
    return rules

def bath_generator(prop_num, type_n_gen,total_depth,def_depth,confusion_num,def_factor, dirc = "./test-data/"):
    top_proposition_list= []
    for i in range(prop_num):
        tmp_proposition_name = proposition_name_generator()
        top_proposition_list.append(tmp_proposition_name)

    type_name = type_n_gen.__name__
    file_path = dirc + type_name+ "-td:" + str(total_depth) + "-df:" + str(round(def_factor*10))+"-cn:"+str(confusion_num)

    total_rules = []
    for p in top_proposition_list:
        rules = type_n_gen(p, total_depth,def_depth,confusion_num)
        total_rules += rules
    save_to_file(total_rules, file_path)

def reset_GLOBAL():
    global PROPOSITION_NAMES
    global RULE_NAMES
    global GROUND_NAMES
    PROPOSITION_NAMES = []
    RULE_NAMES = []
    GROUND_NAMES = []
    return

if __name__== "__main__":

    # def_depth_factor = 0.7
    # total_depth = 5
    # confusion_num = 2
    #
    # def_depth = round(min(def_depth_factor,1) * total_depth)
    # bath_generator(1,type_0_generator,total_depth,def_depth,0)

    gener_list = [type_0_generator,type_1_generator,type_2_generator,type_3_generator]
    total_depths = [10,12,14,16,18,20]
    def_depth_factors = [0.2,0.3,0.4,0.5,0.6,0.7,0.8]
    computsion_num_list = [2,5]
    top_prop_num = 50

    for gen in gener_list:
        for td in total_depths:
            for df in def_depth_factors:
                for cn in computsion_num_list:
                    dd = round(min(df,1) * td)
                    bath_generator(top_prop_num,gen,td,dd,cn,df)
                    reset_GLOBAL()





    # '''
    # Test type 3 generator
    # '''
    # p, undercutter, defeater2, rebutter, defeater3, c = type_3_generator("q", total_depth,def_depth,confusion_num)
    # print (p)
    # print (undercutter)
    # print (defeater2)
    # print (rebutter)
    # print (defeater3)
    # for cc in c :
    #     print (cc)

    '''
    Test type 1 and type 2 generator
    '''
    # p, defeater, defeater2, c = type_2_generator("q", total_depth,def_depth,confusion_num)
    # # p, defeater, defeater2, c = type_1_generator("q", total_depth,def_depth,confusion_num)
    # print (p)
    # print (defeater)
    # print (defeater2)
    # for cc in c :
    #     print (cc)
    '''
    Test type 0 , self warranted paths generator
    '''
    # p, defeater, c = type_0_generator("q", total_depth,def_depth,confusion_num)
    # print (p)
    # print (defeater)
    # for cc in c :
    #     print (cc)

    '''
    Test unwarranted_path_generator
    '''
    # p, defeater = unwarranted_path_generator("q", total_depth,def_depth)
    # print (p)
    # print (defeater)
    # print ("")

    '''
    Test path generator
    '''
    # p = path_generator("t",4,4)
    # print (p)
