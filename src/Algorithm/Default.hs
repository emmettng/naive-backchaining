{-# LANGUAGE FlexibleContexts #-}
module Algorithm.Default where

import           Control.Monad.IO.Class          (MonadIO)
import           Control.Monad.Reader            (MonadReader)

import           Toolkits.Common                 (concat')

import qualified Definitions.Argument   as Arg
import qualified Definitions.ArgumentationSystem as AS 
import qualified Definitions.Enrichment as Enrich
import GHC.RTS.Flags (DebugFlags(weak))


{- Definition : Three default link functions
1. last-link, 2. weakest-link 3.naive-link 
TODO : Is it true that it is always possible to convert semantics expressed by function (computation) to semantics expressed by data type (evidence) ? 
      In other words, with regards to any semantics (relation) , there must be corresponding 
                    functional (computational) expression 
                    and 
                    data type (evidential) expression. 
                    ( any functional expression is equivalent to any data type expression up to isomorphism). 

TODO : update the semantics of link type function in the paper 
    1. collection information from support evidence . 
    2. decide the enrichability of the enrichment , by picking hovering propositions from current support evidence, 
    these hovering propositions need to be furture supported. 
-}
lastLink :: Arg.Info ->  Enrich.Evidence -> (Arg.Info, Enrich.Postulates )
lastLink pInfo evidence =
    let
        defA = [r | r <- evidence , Arg.getLiteralImp r == Arg.D]
        strA = [r | r <- evidence , Arg.getLiteralImp r == Arg.S]
        hoverings = [l |
            l <- concat' $ Arg.getLiteralBody <$> evidence ,
            l `elem` concat (Arg.getLiteralBody <$> strA) ]
    in ( pInfo ++ defA, hoverings)

weakestLink ::Arg.Info ->  Enrich.Evidence -> (Arg.Info, Enrich.Postulates )
weakestLink pInfo evidence =
    let
        defA = [r | r <- evidence , Arg.getLiteralImp r == Arg.D]
        defL = concat $ Arg.getLiteralBody <$> defA
        hoverings = concat' $ Arg.getLiteralBody <$> evidence
    in ( pInfo ++ defA, hoverings) 

naiveLink ::Arg.Info ->  Enrich.Evidence -> (Arg.Info, Enrich.Postulates)
naiveLink pInfo evidence = 
    let postulates = concat' $ Arg.getLiteralBody <$> evidence
    in (pInfo, postulates)

enrichNaive ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.Enrichment -> m [Enrich.Enrichment]
enrichNaive = Enrich.enrich naiveLink

enrichSetNaive ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => [Enrich.Enrichment] -> m [Enrich.Enrichment]
enrichSetNaive = Enrich.enrichSet naiveLink



enrichLast ::    
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.Enrichment -> m [Enrich.Enrichment]
enrichLast = Enrich.enrich lastLink

enrichSetLast ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => [Enrich.Enrichment] -> m [Enrich.Enrichment]
enrichSetLast = Enrich.enrichSet lastLink

enrichWeakest ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.Enrichment -> m [Enrich.Enrichment]
enrichWeakest = Enrich.enrich weakestLink

enrichSetWeakest ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => [Enrich.Enrichment] -> m [Enrich.Enrichment]
enrichSetWeakest = Enrich.enrichSet weakestLink