{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Definitions.Ordering where 

import qualified Data.HashMap.Strict as Map 

import qualified Definitions.Argument as Arg 

data Principle = Eli | Dem deriving Show 

(>=:) :: Arg.Literal -> Arg.Literal -> Bool 
a >=: b = 
    let 
        aRank = Arg.getLiteralRank a 
        bRank = Arg.getLiteralRank b 
    in aRank >= bRank

(<=:) :: Arg.Literal -> Arg.Literal -> Bool 
a <=: b = 
    let 
        aRank = Arg.getLiteralRank a 
        bRank = Arg.getLiteralRank b 
    in aRank <= bRank

{-
Eli success: All          elements in the attacker has priority over at least one element  in defender. 
    [2,2,2] >: [3,2,3] = True 
Dem success: At least one element  in the attacker has priority over all          elements in defender. 
    [2,3,2] >: [3,3,3] = True 
-}
(>:) :: Principle -> Arg.Info -> Arg.Info -> Bool 
(>:) _ _ [] = False  
(>:) _ [] (_:_) = True 
(>:) Eli as bs = 
    let p = (\x -> all ( x <=:) as) <$> bs 
    in or p 
(>:) Dem as bs = 
    let p = (\x -> all ( x >=: ) bs) <$> as 
    in or p 

-- (>:) Eli as (b:bs) = 
--     let satisfy = all (b <=: ) as 
--     in 
--         if not satisfy then (>:) Eli as bs
--         else satisfy 
-- Following definition is incrrect, the base case is not match. 
-- (>:) Dem (a:as) bs = 
--     let satisfy = all (a >=: ) bs 
--     in 
--         if not satisfy then (>:) Dem as bs 
--         else satisfy 