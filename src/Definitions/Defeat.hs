{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
module Definitions.Defeat 
    where

import           Control.Monad.IO.Class          (MonadIO)
import           Control.Monad.Reader            (MonadReader)

import           Run.Env                         (grab)
import           Toolkits.Common                 (splitBoolean)

import qualified Definitions.Argument            as Arg
import qualified Definitions.ArgumentationSystem as AS
import qualified Definitions.Attacks             as Attacks
import qualified Definitions.Enrichment          as Enrich
import qualified Definitions.Ordering            as Ord

data Conflict = Conflict {attacker :: Enrich.Enrichment, defender :: Enrich.Enrichment}

instance Show Conflict where
    show (Conflict attacker defender) =
        "Conflict: " ++ "\n" ++
        "attacker: " ++ show attacker ++ "\n" ++
        "defender: " ++ show defender

data Outcome = Outcome {success :: [Conflict], failure :: [Conflict]} deriving Show

isDefeat ::
    ( AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction
    -> Attacks.Attacks
    -> m [Outcome]
isDefeat link (Attacks.Rebut rebutters ) =  mapM (isRebut link) rebutters
isDefeat _ (Attacks.Undercut undercutters ) =  pure $ isUndercut <$> undercutters
isDefeat link (Attacks.Both rs us) = do
    rebutOutcomes <- mapM (isRebut link) rs
    let undercutOutcomes  = isUndercut <$> us
    pure  $ rebutOutcomes ++ undercutOutcomes

isRebut ::
    ( AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.LinkFunction
    -> Attacks.Rebutter
    -> m Outcome
isRebut link (Attacks.Rebutter lArgument attacker) =  do
    principle <- grab @Ord.Principle
    targetSet <- Enrich.enrichFixA link lArgument
    attackerSet <- Enrich.enrichFixL link attacker
    let
        (winners, losers) = splitBoolean (isPrefer principle)  (Conflict <$> attackerSet <*> targetSet)
    pure $ Outcome
            { success =  winners
            , failure =  losers
            }

isUndercut :: Attacks.Undercutter -> Outcome
isUndercut (Attacks.Undercutter lArgument attacker) =
    let
        targetEnrichment = Enrich.argToEnrichment lArgument
        attackerEnrichment = Enrich.argToEnrichment . Arg.literalToArg $ attacker
        conflict = Conflict attackerEnrichment targetEnrichment
    in Outcome {success = [conflict] , failure = []}

-- isRebut :: Ord.Principle -> Enrich.Enrichment -> Enrich.Enrichment -> Bool
isPrefer :: Ord.Principle -> Conflict -> Bool
isPrefer principle (Conflict attacker defender) =
    let
        attackerInfo = Enrich.getEnrichmentInfo attacker
        defenderInfo = Enrich.getEnrichmentInfo defender
    in  (Ord.>:) principle attackerInfo defenderInfo

restoreDefeater :: Conflict -> Enrich.Enrichment 
restoreDefeater (Conflict attacker _) = Enrich.restoreEnrichability attacker 