{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
module Definitions.Enrichment where

import           Control.Monad.IO.Class          (MonadIO)
import           Control.Monad.Reader            (MonadReader)
import           Data.List                       (intercalate)
import Control.Exception

import qualified Data.HashSet as Set (HashSet, empty, toList, fromList, null,filter, intersection)
import qualified Data.HashMap.Strict as Map 

import           Run.Env                         (grab)
import           Toolkits.Common                 (rmdups)

import           Definitions.Argument            (Argument (aLiterals))
import qualified Definitions.Argument            as Arg
import qualified Definitions.ArgumentationSystem as AS
import qualified Data.Semigroup as Arg
import qualified Data.HashMap.Internal.Strict as Map


{- |Definition : Enrichability 
    Here use Continue to replace Enrichable
    Because in the computation process, each Enrichable argument is accompanied by evidence and will be rendered together as the enriched argument(s),
    the status of the enriched argument is unknown yet. 
-}
data Enrichability = Complete | Pending | Continue deriving (Show , Eq)

{-Definition : Postulate , Evidence , SupportSet -}
type Postulates = [Arg.Literal]
type Evidence = [Arg.Literal]
type Support = [Evidence]

{-Definition : Enrichment-}
data Enrichment = Enrichment
    { getEnrichmentInfo :: Arg.Info
    , getEnrichmentPostulate :: Postulates
    , getEnrichmentArg  :: Arg.Argument
    , getEnrichability :: Enrichability
    } deriving Eq

instance Show Enrichment where
    show (Enrichment info postulates argument enrichability) =
        "< Accumulated Info: " ++ show info ++ "\n" ++
        ", Postulates to be enriched: " ++ show postulates ++ "\n" ++
        ", Argument: " ++ show argument ++ "\n" ++
        ", Enrichability: " ++ show enrichability++ ">\n"

{-Definition : Link function type-}
type LinkFunction = Arg.Info -> Evidence -> (Arg.Info, Postulates)

argToEnrichment :: Arg.Argument -> Enrichment
argToEnrichment arg = Enrichment [] (Arg.aLiterals arg) arg Continue

isComplete :: Enrichment -> Bool
isComplete (Enrichment _ _ _ Complete) = True
isComplete _ = False 

isCircular :: [Arg.Rules] -> [Arg.Literal] -> Bool 
isCircular path postulates = 
    let 
        judgments = Set.fromList $ concatMap (fmap Arg.getLiteralConc) path
        hypothesis = Set.fromList postulates
    in not . Set.null $ Set.intersection judgments hypothesis
        
{-Definition : Enrichiability -}
enrichability ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) =>  Enrichment ->  m (Enrichability, Support)
enrichability (Enrichment _ [] _ _ )= pure (Complete, [])
enrichability (Enrichment _ postulates (Arg.Argument path _ _) _ )= do
    if path `isCircular` postulates then pure (Pending, []) --A : if Circular detected , return pending
    else do 
        supports <- mapM concludeBy' postulates                                 
        if or (null <$> supports)
            then pure (Pending, [])  -- A: if cannot find support for all postulates, return pending (All or Noting).
            else
                do
                    let supportSet = foldr createParallel [[]] supports
                    pure (Continue, supportSet )
    where
        concludeBy' ::
            ( AS.Has AS.DefeasibleTheory env
            , MonadIO m
            , MonadReader env m
            ) =>  Arg.Literal -> m Arg.Rules
        concludeBy' l = do
            rulesDict <- AS.getDefeasibleTheory <$> grab @AS.DefeasibleTheory
            pure $ Map.findWithDefault [] l rulesDict 

        createParallel :: [a] -> [[a]] -> [[a]]
        createParallel paths ls = do
                pa <- paths
                a <- ls
                pure $  pa:a

{-Begin Definition : enrich
combinator of two functions:
1. enrichability (createParallel).
2. link type function.
-}
enrich ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => LinkFunction -> Enrichment -> m [Enrichment]
enrich _ e@(Enrichment info postulates arg Complete) = pure [e]
enrich _ e@(Enrichment info postulates arg Pending) = pure [e] 
enrich link e@(Enrichment info postulates arg Continue) = do
    enrichable <- enrichability e
    case enrichable of
        (Complete, _) -> pure [e{getEnrichability=Complete}]
        (Pending, _)  -> pure [e{getEnrichability=Pending}]
        (Continue, supportSet) ->
          let
            enrichResults = combinator' <$> supportSet
          in pure $ wrapEnrichment <$> enrichResults
    where
        combinator'  :: Evidence -> (Arg.Info, Postulates, Arg.Argument, Enrichability)
        combinator' evidence =
          let
            newArgument = enrichArgument'' arg evidence
            (newInfo , newPostulate) = link info evidence
          in (newInfo , newPostulate, newArgument, Continue)
        enrichArgument'' :: Arg.Argument -> Evidence -> Arg.Argument
        enrichArgument'' (Arg.Argument aPath aLiterals aConc) evidence =
          Arg.Argument { Arg.aPath = aPath ++ [evidence]
                       , Arg.aLiterals = mconcat $ Arg.getLiteralBody <$> evidence              -- B: here the semantics is corresponding to A
                       , Arg.aConc = aConc
                       }
        wrapEnrichment :: (Arg.Info , Postulates , Arg.Argument , Enrichability) -> Enrichment 
        wrapEnrichment (info, postulates , arg, enrichability) = Enrichment info postulates arg enrichability 

{- Auxiliary Functions below -}
restoreEnrichability :: Enrichment -> Enrichment 
restoreEnrichability e@(Enrichment _ _ arg _)  = e{getEnrichability = Continue, getEnrichmentPostulate = Arg.aLiterals arg}

enrichSet ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => LinkFunction -> [Enrichment] -> m [Enrichment]
enrichSet link enrichSet = do
    rs <- mapM (enrich link) enrichSet
    pure $ concat rs

enrichFix ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => LinkFunction -> [Enrichment] -> m [Enrichment]
enrichFix link toEnrich =
    let
        unComplete = filter (not . isComplete)  toEnrich
        complete = filter isComplete  toEnrich
    in
        if not . null $ unComplete then
            do
                enriched <- enrichSet link unComplete
                fixPont <- enrichFix link enriched
                pure $ complete ++ fixPont
        else pure complete

{-  Fix point of enrich function, given a literal-}
enrichFixL ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => LinkFunction -> Arg.Literal -> m [Enrichment]
enrichFixL link literal = do
        enrichSet <- enrich link $ argToEnrichment . Arg.literalToArg $ literal
        enrichFix link enrichSet

{-  Fix point of enrich function, given an Argument -}
enrichFixA ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => LinkFunction -> Arg.Argument -> m [Enrichment]
enrichFixA link argument = do
        enrichSet <- enrich link $ argToEnrichment argument
        enrichFix link enrichSet

{-  Fix point of enrich function, given an enrichment -}
enrichFixE ::
    ( AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    ) => LinkFunction -> Enrichment -> m [Enrichment]
enrichFixE link enrichment = do
        enrichSet <- enrich link enrichment
        enrichFix link enrichSet
