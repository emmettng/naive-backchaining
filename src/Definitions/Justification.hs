{-# LANGUAGE FlexibleContexts #-}
module Definitions.Justification where 

import           Control.Monad.IO.Class          (MonadIO, liftIO)
import           Control.Monad.Reader            (MonadReader)
import           Data.Either                     (partitionEithers, lefts, rights, isRight)

import qualified Definitions.Attacks as Attacks 
import qualified Definitions.Defeat as Defeat 
import qualified Definitions.Ordering as Ord
import qualified Definitions.Enrichment as Enrich 
import qualified Definitions.Argument as Arg
import qualified Definitions.ArgumentationSystem as AS
import qualified Algorithm.Default as Default


data Warranted = SelfWarranted [Enrich.Enrichment]  | Warranted Enrich.Enrichment Unwarranted   deriving Show

data Unwarranted = Unwarranted [(Enrich.Enrichment, Warranted)] | Circle  deriving Show

data Failures = DefeatCircle

type AttackerRecord = (Attacks.Attacks, Enrich.Enrichment)
type AttackerRecords = [AttackerRecord]

type DefeaterRecord = ([Enrich.Enrichment], Enrich.Enrichment )
type DefeaterRecords = [DefeaterRecord]

data Justified = IsJustified | NotJustified deriving Show 

{- TODO : define a higher level structure so that we could descriptively 
    define justification criteria based on 
    1. relations: attacks , defeated 
    2. WFS vs DFS 
    etc.
-}
isJustified ::( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction -> Enrich.Enrichment -> m Justified 
isJustified link en = isDefeated link [en]

isDefeated ::   ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction -> [Enrich.Enrichment] -> m Justified 
isDefeated link enrichments = do 
    attackerStatus <- checkAttackers enrichments
    case attackerStatus of
        Right warrantedEnriche -> pure IsJustified 
        Left attackerRecords -> do
            defeaterStatus <- checkDefeaters link attackerRecords
            case defeaterStatus of 
                (defeaters', new@(_:_)) -> do 
                            isDefeated link new 
                (defeaters', [])  -> pure NotJustified

checkAttackers ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => [Enrich.Enrichment]
--    -> m (Either [(Attacks.Attacks,Enrich.Enrichment)] [Enrich.Enrichment])     -- find all complete SF enrichment
    -> m (Either AttackerRecords [Enrich.Enrichment])         -- find one complete SF enrichment
checkAttackers enrichments = do 
  ers <- Default.enrichSetNaive enrichments 
  rs <- mapM checkAttacker ers
  let
    fs = filter isRight rs 
  case fs of 
    [] -> pure . Left . concat . lefts $ rs
    _ -> pure . Right . rights $ rs 

checkAttacker :: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.Enrichment -> m (Either AttackerRecords Enrich.Enrichment )
checkAttacker enrichment = endPointCheck [enrichment] [] 

endPointCheck:: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    )=> [Enrich.Enrichment] -> AttackerRecords -> m (Either AttackerRecords Enrich.Enrichment )
endPointCheck [] aRs = pure $ Left aRs
endPointCheck (e:es) aRs = do 
    r <- Attacks.attackers' e 
    case r of 
      Attacks.Peace -> 
        if Enrich.isComplete e then pure $ Right e 
        else do 
            toBeChecked <- Default.enrichNaive e
            endPointCheck (toBeChecked ++ es) aRs 
      Attacks.Conflict a-> endPointCheck es ((a,e) :aRs)


checkDefeaters ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction 
    -> AttackerRecords
    -> m ( DefeaterRecords
         , [Enrich.Enrichment]
         )
checkDefeaters link ars = traverseAttackers link ars [] [] 
  where 
    traverseAttackers :: 
      ( AS.Has AS.L env
      , AS.Has AS.DefeasibleTheory env
      , AS.Has Ord.Principle env
      , MonadIO m
      , MonadReader env m
      ) 
      => Enrich.LinkFunction
      -> AttackerRecords
      -> DefeaterRecords 
      -> [Enrich.Enrichment] 
      -> m (DefeaterRecords, [Enrich.Enrichment])
    traverseAttackers link [] dfs fes = pure $ (dfs,fes) 
    traverseAttackers link ((a,e):as) dfs fes = do 
      outcomes <- Defeat.isDefeat link a 
      let suc = concat $ Defeat.success <$> outcomes 
      if null suc 
        then traverseAttackers link as dfs (e:fes)
        else traverseAttackers link as ((Defeat.attacker <$> suc ,e) : dfs) fes 