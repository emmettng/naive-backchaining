{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE TypeApplications #-}

module Definitions.Attacks where

import           Control.Monad.IO.Class          (MonadIO)
import           Control.Monad.Reader            (MonadReader)
import           Data.Maybe                      (catMaybes)

import           Run.Env                         (grab)
import qualified Data.HashSet as Set 

import qualified Definitions.Argument            as Arg
import qualified Definitions.ArgumentationSystem as AS
import qualified Definitions.Enrichment          as Enrich

data Rebutter = Rebutter Arg.Argument Arg.Literal deriving (Show, Eq)
type Rebutters = [Rebutter]

data Undercutter = Undercutter Arg.Argument Arg.Literal deriving (Show, Eq)
type Undercutters = [Undercutter]

{- TODO : The role of human 
    Here, the definition of Attack should not include Peace, 
    because , semantically, usually, Peace/Attack lead to different following operation. 
    If this is the end of the computation, it is fine, no information get lost.
    If this is a part of larger computation (composition), it will bring unnecessary pattern match (branches). 
    thus increase the complexity of the program structure which specified by this thoughtless design. 
1. alter this may help modularize the entire DFS and WFS strategy. 
2. The role of us VS the polymorphic modularization 
-}
data Attacks where
    Rebut :: Rebutters -> Attacks
    Undercut :: Undercutters -> Attacks
    Both ::  Rebutters -> Undercutters -> Attacks
    deriving (Show, Eq)

{- Note : Sum and Product in ADT can be treated as search branches in DFS/WFS . INTERESTING -}
data ConflictStatus = Conflict Attacks | Peace  deriving Show

(>&&<) :: Rebutters -> Undercutters -> ConflictStatus
[]      >&&< []         = Peace
r@(_:_) >&&< []         = Conflict $ Rebut r
[]      >&&< u@(_:_)    = Conflict $ Undercut u
r@(_:_) >&&< u@(_:_)    = Conflict $ Both r u

{- Definition : rebut
TODO : 1. How this can be defined as a type , just like 'leq'.
2. we take only literal rebut argument , how about other cases ? necessary ?
3. Defeasible rules are computed twice (rebut and undercut), could be optimized .
-}

{- Enrich related definitions: rebut, undercuts, attackers
use 'last path' , the most recent enriched rules are involved in the computation.

TODO :
    1. here each pair of argument and literal can locate at most one Rebutter
    2. Enrich.argToEnrichment . Arg.literalToArg $ a , a can only be a proposition here .
-}
rebut' ::  Arg.Argument -> Arg.Literal -> [Maybe Rebutter]
rebut'  (Arg.Argument [] _ _) _ = [Nothing]
rebut'  (Arg.Argument path _ _) a@(Arg.Atom _) =
    let defRules = [ r | r <- last path, Arg.getLiteralImp r == Arg.D]
    in do
        r <- defRules
        let attackPoint = Arg.getLiteralConc r
        pure $
            if neg a == attackPoint
                then Just (Rebutter (Arg.literalToArg attackPoint)  a)
                else Nothing
rebut' _ _ = [Nothing]

{- TODO:
    `Arg.literalToArg. Arg.getLiteralConc` is how sub argument should be defined .
    add definition of sub-argument in the paper.
-}
undercut' ::  Arg.Argument -> Arg.Literal -> [Maybe Undercutter]
undercut'  (Arg.Argument [] _ _) _ = [Nothing]
undercut'  (Arg.Argument path _ _) a@Arg.Rule{}=
    let defRules = [ r | r <- last path, Arg.getLiteralImp r == Arg.D]
    in do
        r <- defRules
        pure $
            if neg a == r
                then Just ( Undercutter
                            (Arg.literalToArg . Arg.getLiteralConc $ r)
                            a
                          )
                else Nothing
undercut' _ _ = [Nothing]

attackers' ::
    ( AS.Has AS.L env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.Enrichment -> m ConflictStatus
attackers' (Enrich.Enrichment _ _ arg _) = do
    literals <- Set.toList <$> AS.getLanguage <$> grab @AS.L
    let
        rebutters = catMaybes . concat $ rebut' arg <$> literals
        undercutters = catMaybes . concat $ undercut' arg <$> literals
    pure $ rebutters >&&< undercutters

neg :: Arg.Literal -> Arg.Literal
neg (Arg.Rule n a i h r)
    |  head n == '!' =
        let nLiteral = tail n
        in Arg.Rule nLiteral a i h r
    | otherwise =
        let nLiteral = '!' : n
        in Arg.Rule nLiteral a i h r
neg (Arg.Atom n )
    |  head n  == '!' =
        let nLiteral = tail n
        in Arg.Atom nLiteral
    | otherwise =
        let nLiteral = '!' : n
        in Arg.Atom nLiteral

{- | attackers''
    Compute attackers based on rules of the entire enrichment.
-}
rebut'' ::  Arg.Argument -> Arg.Literal -> [Maybe Rebutter]
rebut''  (Arg.Argument path _ _) a@(Arg.Atom _) =
    let defRules = [ r | r <- concat path, Arg.getLiteralImp r == Arg.D]
    in do
        r <- defRules
        let attackPoint = Arg.getLiteralConc r
        pure $
            if neg a == attackPoint
                then Just (Rebutter (Arg.literalToArg attackPoint)  a)
                else Nothing
rebut'' _ _ = [Nothing]

undercut'' ::  Arg.Argument -> Arg.Literal -> [Maybe Undercutter]
undercut''  (Arg.Argument path _ _) a@Arg.Rule{}=
    let defRules = [ r | r <- concat path, Arg.getLiteralImp r == Arg.D]
    in do
        r <- defRules
        pure $
            if neg a == r
                then Just ( Undercutter
                            (Arg.literalToArg . Arg.getLiteralConc $ r)
                            a
                          )
                else Nothing
undercut'' _ _ = [Nothing]

attackers'' ::
    ( AS.Has AS.L env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.Enrichment -> m ConflictStatus
attackers'' (Enrich.Enrichment _ _ arg _) = do
    literals <- Set.toList <$> AS.getLanguage <$> grab @AS.L
    let
        rebutters = catMaybes . concat $ rebut'' arg <$> literals
        undercutters = catMaybes . concat $ undercut'' arg <$> literals
    pure $ rebutters >&&< undercutters

-- newtype Rebutters = Rebutters [Arg.Literal] deriving Show
-- newtype Undercutters = Undercutters [Arg.Literal] deriving Show
-- instance Eq Rebutters where
--     Rebutters [] == Rebutters (_:_) = False
--     Rebutters (_:_) == Rebutters [] = False
--     Rebutters xs == Rebutters ys = xs `listEq` ys
--
-- instance Eq Undercutters where
--     Undercutters [] == Undercutters (_:_) = False
--     Undercutters (_:_) == Undercutters [] = False
--     Undercutters xs == Undercutters ys = xs `listEq` ys

-- listEq :: (Eq a) => [a] -> [a] -> Bool
-- listEq xs ys =
--         let
--             lenEq = length xs == length ys
--             elemEqL = and [x `elem` ys | x <- xs]
--             elemEqR = and [y `elem` xs | y <- ys]
--         in lenEq && elemEqL && elemEqR

-- instance Eq Attacks where
--     Peace == Peace = True
--     (Rebut l) == (Rebut r) = l == r
--     (Undercut l) == (Undercut r) = l == r
--     (Both lr lu) == (Both rr ru) = (lr == rr) && (lu == ru)
--     _ == _ = False

{- Ordinary definitions : rebut , undercut, attackers
use 'concat path'  all rules of an argument are involved in the computation.
-}
-- rebut ::  Arg.Argument -> Arg.Literal -> Bool
-- rebut  (Arg.Argument path _ _) a@(Arg.Atom _) =
--     let defConc =  [Arg.getLiteralConc r | r <- concat path, Arg.getLiteralImp r == Arg.D]
--     in neg a `elem` defConc
-- rebut _ _ = False

-- {-TODO : This definition here is Wrong
-- should not be 'r@Arg.Rule{}'
-- -}
-- undercut ::  Arg.Argument -> Arg.Literal -> Bool
-- undercut  (Arg.Argument path _ _) r@Arg.Rule{} =
--     let defRules = [ r | r <- concat path, Arg.getLiteralImp r == Arg.D]
--     in neg r `elem` defRules
-- undercut _ _ = False

-- attackers ::
--     ( AS.Has AS.L env
--     , MonadIO m
--     , MonadReader env m
--     ) => Enrich.Enrichment -> m Attacks
-- attackers (Enrich.Enrichment _ _ arg) = do
--     literals <- AS.getLanguage <$> grab @AS.L
--     let
--         rebutters = Rebutters $ filter (rebut arg) literals
--         undercutters = Undercutters $ filter (undercut arg) literals
--     pure $ rebutters >&&< undercutters
