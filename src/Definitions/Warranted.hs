{-# LANGUAGE FlexibleContexts #-}
module Definitions.Warranted where


import           Control.Monad.IO.Class          (MonadIO, liftIO)
import           Control.Monad.Reader            (MonadReader)
import           Data.Either                     (partitionEithers, lefts, rights, isRight)
import           Data.Bifunctor                  (first)

import qualified Definitions.Argument as Arg
import qualified Definitions.ArgumentationSystem as AS
import qualified Definitions.Enrichment as Enrich
import qualified Definitions.Attacks as Attacks
import qualified Definitions.Ordering as Ord
import qualified Definitions.Defeat as Defeat

import qualified Algorithm.Default as Default
import qualified Data.HashMap.Internal.Strict as Map
import qualified Definitions.Defeat as Conflict
import Definitions.Defeat (Conflict)
import qualified Definitions.ArgumentationSystem as Enrich

data Warranted = SelfWarranted [Enrich.Enrichment]  | Warranted Enrich.Enrichment Unwarranted   deriving Show

data Unwarranted = Unwarranted [(Enrich.Enrichment, Warranted)] | Circle  deriving Show

data Failures = DefeatCircle

type AttackerRecord = (Attacks.Attacks, Enrich.Enrichment)
type AttackerRecords = [AttackerRecord]

type DefeaterRecord = ([Enrich.Enrichment], Enrich.Enrichment )
type DefeaterRecords = [DefeaterRecord]

type LayerDict = Map.HashMap String Int 

data SearchLog = 
  SearchLog { enrichSet :: [Enrich.Enrichment]
            , layDict :: LayerDict 
            , defeaterRecords :: DefeaterRecords 
            , warrantedDefeaters :: [(Enrich.Enrichment,Warranted)]
  }deriving Show 

{- TODO : an exception is defeated relation could form a circle
    1. How this circle can be realized as I give the definition of Warranted ?
    2. Similar issue such as the definition of enrichment which is different from descrbed in the paper ?
    3. Is there any other similar concept that missed in this definition ?
-}
isJustified ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction -> Enrich.Enrichment -> m (Either Unwarranted Warranted)
isJustified link queryEnrichment = do
  let
    searchLog =initializeSearchLog queryEnrichment 
  isWarranted link searchLog

isWarranted ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction -> SearchLog -> m (Either Unwarranted Warranted)
isWarranted link sl@(SearchLog enrichments layerDict defeaters wds )= do
  -- liftIO $ putStrLn ""
  
  -- liftIO $ print enrichments

  attackerStatus <- checkAttackers enrichments
  
  -- liftIO $ print attackerStatus  

  case attackerStatus of
    Right warrantedEnriche -> pure . Right $ SelfWarranted warrantedEnriche

    Left attackerRecords -> do
      defeaterStatus <- checkDefeaters link attackerRecords

      -- liftIO $ print defeaterStatus

      case defeaterStatus of            
      -- correspond to section between Self-warranted (bullet point 1) and detecting warranted (bullet point 2).

        (defeaters', new@(_:_)) -> do 
          isWarranted link $ SearchLog new layerDict (defeaters' ++ defeaters) wds 

        (defeaters', [])  -> do
          let
            allDefeaters = defeaters' ++ defeaters
          warrantyResult <- unwarrantedDetect link layerDict allDefeaters  -- This is the section of bullet point 2.
          
          -- liftIO $ print warrantyResult

          case warrantyResult of
            ([], [], wds' ) -> pure . Left . Unwarranted $ wds ++ wds' 
            (new', defeaters'', wds') ->do 
              isWarranted link $ SearchLog new' layerDict defeaters'' (wds ++ wds')

isWarranted' ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction -> SearchLog -> m (Either Unwarranted Warranted)
isWarranted' link sl@(SearchLog enrichments layerDict defeaters wds )= do
  -- liftIO $ putStrLn ""
  
  -- liftIO $ print enrichments

  attackerStatus <- checkAttackers' enrichments

  -- liftIO $ print attackerStatus  

  case attackerStatus of
    Right warrantedEnriche -> pure . Right $ SelfWarranted warrantedEnriche

    Left attackerRecords -> do
      defeaterStatus <- checkDefeaters link attackerRecords

      -- liftIO $ print defeaterStatus

      case defeaterStatus of            -- correspond to section between Self-warranted (bullet point 1) and detecting warranted (bullet point 2).

        (defeaters', new@(_:_)) -> do 
          isWarranted link $ SearchLog new layerDict (defeaters' ++ defeaters) wds 

        (defeaters', [])  -> do
          let
            allDefeaters = defeaters' ++ defeaters
          warrantyResult <- unwarrantedDetect link layerDict allDefeaters  -- This is the section of bullet point 2.
          
          -- liftIO $ print warrantyResult

          case warrantyResult of
            ([], [], wds' ) -> pure . Left . Unwarranted $ wds ++ wds' 
            (new', defeaters'', wds') ->do 
              isWarranted link $ SearchLog new' layerDict defeaters'' (wds ++ wds')

unwarrantedDetect :: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction  -> LayerDict -> DefeaterRecords -> m ( [Enrich.Enrichment]
                            , DefeaterRecords
                            , [(Enrich.Enrichment,Warranted)]
                            )
unwarrantedDetect link layerDict dRecords = traverseDefeaters link [] dRecords [] 
  where 
    traverseDefeaters :: 
      ( AS.Has AS.L env
      , AS.Has AS.DefeasibleTheory env
      , AS.Has Ord.Principle env
      , MonadIO m
      , MonadReader env m
      )
      => Enrich.LinkFunction -> [Enrich.Enrichment] -> DefeaterRecords -> [(Enrich.Enrichment, Warranted)]
        -> m ( [Enrich.Enrichment]
             , DefeaterRecords
             , [(Enrich.Enrichment,Warranted)]
             )
    traverseDefeaters link [] [] wds = pure ([] , [], wds) 
    traverseDefeaters link xs@(_:_) drs wds = pure (xs,drs,wds)
    traverseDefeaters link _ (d:ds) wds = do 
      let 
        (defeaters, e)= first (Enrich.restoreEnrichability <$> ) d 
        -- enrichableDefeaters = (\(es,e) -> (Enrich.restoreEnrichability <$> es ,e)) d 
        searchLog = initializeSearchLogSet layerDict defeaters 
      c <- isWarranted' link searchLog
      case c of  
        Right wd -> traverseDefeaters link [] ds ((e,wd) : wds)
        Left _ -> traverseDefeaters link [e] ds wds           
        -- Unwarranted defeaters is discarded above, thus only SF warranted. 
        -- How to show the proof of ordinary warranted defeater is a problem. 

initializeSearchLog :: Enrich.Enrichment -> SearchLog
initializeSearchLog e = SearchLog 
                          [e] 
                          Map.empty  
                          []
                          []

initializeSearchLogSet :: LayerDict -> [Enrich.Enrichment] -> SearchLog
initializeSearchLogSet layerDict es = SearchLog es Map.empty [] []

-- -- selectSFenrichment :: Enrich.Enrichment -> Warranted
-- wrapperSWenrichment :: [Enrich.Enrichment] -> Warranted
-- wrapperSWenrichment [x] = SelfWarranted x

-- wrapperSFenrichment = undefined

checkDefeaters ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction 
    -> AttackerRecords
    -> m ( DefeaterRecords
         , [Enrich.Enrichment]
         )
checkDefeaters link ars = traverseAttackers link ars [] [] 
  where 
    traverseAttackers :: 
      ( AS.Has AS.L env
      , AS.Has AS.DefeasibleTheory env
      , AS.Has Ord.Principle env
      , MonadIO m
      , MonadReader env m
      ) 
      => Enrich.LinkFunction
      -> AttackerRecords
      -> DefeaterRecords 
      -> [Enrich.Enrichment] 
      -> m (DefeaterRecords, [Enrich.Enrichment])
    traverseAttackers link [] dfs fes = pure $ (dfs,fes) 
    traverseAttackers link ((a,e):as) dfs fes = do 
      outcomes <- Defeat.isDefeat link a 
      let suc = concat $ Defeat.success <$> outcomes 
      if null suc 
        then traverseAttackers link as dfs (e:fes)
        else traverseAttackers link as ((Defeat.attacker <$> suc ,e) : dfs) fes 


{-
Left :
  Attacks : subargument being attack , attacker liter
  Enrichment : The enrichment that contains this sub-argument.
Right :
  [Enrichment] : Enrichments that not being attacked
-}
checkAttackers ::
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => [Enrich.Enrichment]
--    -> m (Either [(Attacks.Attacks,Enrich.Enrichment)] [Enrich.Enrichment])     -- find all complete SF enrichment
    -> m (Either AttackerRecords [Enrich.Enrichment])         -- find one complete SF enrichment
checkAttackers enrichments = do 
  ers <- Default.enrichSetNaive enrichments 
  rs <- mapM checkAttacker ers
  let
    fs = filter isRight rs 
  case fs of 
    [] -> pure . Left . concat . lefts $ rs
    _ -> pure . Right . rights $ rs 

checkAttacker :: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.Enrichment -> m (Either AttackerRecords Enrich.Enrichment )
checkAttacker enrichment = endPointCheck [enrichment] [] 

endPointCheck:: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    )=> [Enrich.Enrichment] -> AttackerRecords -> m (Either AttackerRecords Enrich.Enrichment )
endPointCheck [] aRs = pure $ Left aRs
endPointCheck (e:es) aRs = do 
    r <- Attacks.attackers' e 
    case r of 
      Attacks.Peace -> 
        if Enrich.isComplete e then pure $ Right e 
        else do 
            toBeChecked <- Default.enrichNaive e
            endPointCheck (toBeChecked ++ es) aRs 
      Attacks.Conflict a-> endPointCheck es ((a,e) :aRs)

checkAttackers' :: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    )
    => [Enrich.Enrichment] -> m (Either AttackerRecords [Enrich.Enrichment] )
checkAttackers' es = do 
  rs <- mapM checkAttacker' es 
  let
    fs = filter isRight rs 
  case fs of 
    [] -> pure . Left . concat . lefts $ rs
    _ -> pure . Right . rights $ rs   

checkAttacker' :: 
    ( AS.Has AS.L env
    , AS.Has AS.DefeasibleTheory env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.Enrichment -> m (Either AttackerRecords Enrich.Enrichment )
checkAttacker' e = do 
  initialAttacks <- Attacks.attackers'' e
  case initialAttacks of 
    Attacks.Peace -> do 
        toBeChecked <- Default.enrichNaive e
        endPointCheck toBeChecked [] 
    Attacks.Conflict a -> pure . Left $ [(a, e)]