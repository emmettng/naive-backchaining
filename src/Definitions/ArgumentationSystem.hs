{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}


module Definitions.ArgumentationSystem where

import qualified Data.HashMap.Strict  as Map

import           Definitions.Argument as Arg (Argument, Info, Literal, Name,
                                              Rules)
import           Definitions.Ordering as Ord (Principle)
import qualified Data.HashSet as Set 

type PreferenceMap = Map.HashMap Name Int


-- type LinkFunction = Arg.Info -> Arg.Argument -> Arg.Rules -> (Arg.Info, Arg.Argument)

newtype L = L {getLanguage :: Set.HashSet Arg.Literal }
instance Show L where
    show (L literals) = show literals

newtype DefeasibleTheory = DefeasibleTheory {getDefeasibleTheory :: Map.HashMap Arg.Literal [Arg.Literal]}
instance Show DefeasibleTheory where
    show (DefeasibleTheory rules) = show rules

data AS = AS
    { asRules          :: DefeasibleTheory
    , asLiterals       :: L
    , asPreferenceMap  :: PreferenceMap
    , asOrderPrinciple :: Ord.Principle
    }

class Has field context where
    obtain :: context -> field

instance Has DefeasibleTheory              AS          where obtain = asRules
instance Has DefeasibleTheory              DefeasibleTheory where obtain = id               -- This will be used in QuickCheck test or other property test.

instance Has L                  AS          where obtain = asLiterals

instance Has PreferenceMap  AS          where obtain = asPreferenceMap
instance Has Ord.Principle  AS          where obtain = asOrderPrinciple

instance Show AS where
    show (AS rules literals prefMap principle)=
        "Argumentation System : " ++ "\n" ++
        "\t" ++ "Rules: " ++ show rules ++ "\n" ++
        "\t" ++ "Literals : " ++ show literals ++ "\n" ++
        "\t" ++ "Preferencem Map: " ++ show prefMap ++ "\n" ++
        "\t" ++ "Ordering Principle: " ++ show principle

