{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Run.Env where

import           Control.Monad.Reader            (MonadIO, MonadReader,
                                                  ReaderT (..), asks)
-- import Control.Monad.Writer ( MonadIO, WriterT(..) )                                                 


import           Definitions.ArgumentationSystem (AS(..), Has (..))

grab :: forall field env m . (MonadReader env m, Has field env) => m field
grab = asks $ obtain @field

newtype App a = App 
    { unApp :: ReaderT AS IO a
    } deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader AS) 

runApp :: AS -> App a -> IO a 
runApp as app = (runReaderT $ unApp app) as 

-- newtype AppWithLog a = AppWithLog
--     { unAppWithLog :: ReaderT AS (WriterT String IO) a
--     } deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader AS) 

-- runAppWithLog :: AS -> AppWithLog a -> IO (a , String) 
-- runAppWithLog as app = runWriterT $ (runReaderT $ unAppWithLog app) as 