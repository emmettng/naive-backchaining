module Run.Helper where

import           Control.Monad.IO.Class          (MonadIO)
import           Control.Monad.Reader            (MonadReader)


import           Run.Env                         (App, grab, runApp)

import qualified Definitions.Argument            as Arg
import qualified Definitions.ArgumentationSystem as AS 
import qualified Parser.FileParser               as FP

ghciRunner :: AS.AS -> App a -> IO a 
ghciRunner = runApp 

-- getRunner :: FilePath -> App () b -> IO b
-- getRunner filePath func = do
--     env <- FP.parseDefaultEnv filePath
--     runApp env func
-- 
-- bugRunner :: FilePath -> IO (App () b-> IO b)
-- bugRunner filePath = do
--     env <- FP.parseDefaultEnv filePath
--     pure $ runApp env
-- 
-- getProposition :: FilePath -> String  -> IO(D.Literal ())
-- getProposition filePath pro = do
--     env <- FP.parseDefaultEnv filePath
--     let
--         runner = runApp env
--         lMap = FP.parseLiteralMap env
--     pure $ FP.parseQueryLiteral pro lMap
