module Toolkits.Common where 

import Data.List (group, sort)

rmdups :: Eq a => [a] -> [a]
rmdups = rdHelper []
    where rdHelper seen [] = seen
          rdHelper seen (x:xs)
              | x `elem` seen = rdHelper seen xs
              | otherwise = rdHelper (seen ++ [x]) xs

concat' :: Eq a => [[a]] -> [a]
concat' = rmdups . concat 

{-| True set : fst , False set : snd -}
splitBoolean :: (a -> Bool) -> [a] -> ([a],[a])
splitBoolean f xs = split' f xs ([],[])
    where 
        split' :: (a -> Bool) -> [a] -> ([a],[a]) -> ([a],[a])
        split' f (x:xs) (ts, fs) = 
            if f x 
                then split' f xs (x:ts, fs) 
                else split' f xs (ts, x:fs)
        split' f [] tts = tts 
