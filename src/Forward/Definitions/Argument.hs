{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveAnyClass #-}

module Forward.Definitions.Argument where 

import           Control.Monad (guard)
import           Data.Hashable (Hashable)

{- Begin: Basic Types -}
type Name = String

-- | Implication type
-- 'S' : Strict
-- 'D' : Defeasible
-- 'N' : Not Applicable
data Imp = S | D | N 

instance Show Imp where
    show S = "->"
    show D = "=>"
    show N = " "

instance Eq Imp where
    (==) S S = True
    (==) D D = True
    (==) N N = True
    (==) _ _ = False

type Rank = Int 
{- End : Basic Types -}

{- Definition : Literal -}
{- TODO :
    1. Atom and Rule may be need to be defined separately.
    2. getLiteralImp , get LiteralBody should only exert on rules other than atom literals.
-}
data Literal where
    Atom :: Name -> Literal
    Rule :: Name -> [Literal] -> Imp -> Literal -> Rank -> Literal 

type Rules = [Literal]
type Path = [Rules]

{-Definition : Info
    TODO: Info could also be an instance of Monoid so that we might abstract how info appending each other.
    Info may need to be defined in somewhere else. 
-}
type Info = [Literal]

getLiteralName :: Literal -> Name
getLiteralName (Atom name)       = name
getLiteralName (Rule name _ _ _ _) = name

getLiteralBody :: Literal -> [Literal]
getLiteralBody (Rule _ body _ _ _) = body
getLiteralBody (Atom _)          = []

getLiteralImp :: Literal -> Imp
getLiteralImp (Rule _ _ i _ _) = i
getLiteralImp (Atom _ )       = N

getLiteralConc :: Literal -> Literal
getLiteralConc (Rule _ _ _ head _) = head
getLiteralConc h@(Atom _ )        = h

{- TODO : Atom and Rule should be defined separately if possible. 
    Try later . 
-}
getLiteralRank :: Literal -> Rank 
getLiteralRank (Rule _ _ _ _ rank) = rank 
getLiteralRank (Atom _ )        = 0

instance  Show Literal where
    show (Rule name body imp head rank) = 
            "( " ++ name ++ ": " ++ body'names ++ imp'name ++ head'name ++ 
            ", " ++ show rank ++ " )"
        where
            body'names  = unwords $ getLiteralName <$> body
            imp'name    = show imp
            head'name   = getLiteralName head
    show (Atom name) = name

{- TODO :
    1. bodyEq need to be improved .
-}
instance Eq Literal where
    (Atom a1) == (Atom a2) = a1 == a2
    (Rule name1 body1 imp1 head1 rank1) == (Rule name2 body2 imp2 head2 rank2) =
        let
            nameEq  = name1 == name2
            bodyEq  = (length body1 == length body2)
            impEq   = imp1 == imp2
            headEq  = head1 == head2
        in nameEq && bodyEq && impEq && headEq
    (Atom _ ) == Rule{} = False
    Rule{} == Atom{} = False

{- Definition : Argument that draws more than one conclusion
  TODO :
    1. aLiterals take Rule only .
    2. aConc takes Atom only .
    3. aConc can not be empty
    4. path could be empty but not when aLiterals is empty.
  TODO :
    Rewrite definition of Argument , literal argument should be the base case.
    so that the memeber of type Argument can not be invalid. Current definition cannot stop
    the declariation of invalid argument (randomly combining rule and literals).
-}
data Argument = 
    Argument { aPath :: Path
             , aPathLog :: [Literal]
             , aLiterals :: [Literal]
             , aHoverings :: [Literal]
             , aConc :: Literal 
             , aInfo :: [Literal]
             } deriving Eq

-- instance Show Argument where
--     show (Argument path literals c ) =
--         "{ Conclusions : " ++ show c ++
--         ", path : " ++ show path ++
--         ", Literals : " ++ show literals ++ " }"
-- 
-- {- literal here can only be atom, cannot start from Rule because we do not know
-- what link function will be used.
-- TODO: constraint that literal can only be Atom.
-- -}
-- literalToArg :: Literal -> Argument
-- literalToArg l = Argument [] [l] l
-- 