{-# LANGUAGE OverloadedStrings #-}
module Parser.FileParser where

import           Control.Monad        (guard)
import qualified Data.HashMap.Strict  as Map
import qualified Data.HashSet   as Set
import           Data.List.Split      (splitOn)
import           Data.Maybe           (fromJust, fromMaybe)
import           Data.Text            (isInfixOf, pack)
import           System.IO

import qualified Definitions.Argument as Arg (Rules(..),Imp (..), Literal (..), Name,
                                              getLiteralBody, getLiteralConc,
                                              getLiteralImp, getLiteralName)
import qualified Definitions.Attacks as Conf (neg)
import qualified Definitions.ArgumentationSystem as AS 
import qualified Definitions.Ordering as Ord 

import           Toolkits.Common      (rmdups)
import qualified Algorithm.Default as Default

{- e.g :
    r79:p60,p61->!r71,1
    ruleName = "r79"
    premisesName = ["p60","p61"]
    impName = "->"
    conclusionName = "!r71"
    preferName = "1"
-}
data Knowledge = Knowledge
    { ruleName       :: String
    , premisesName   :: [String]
    , impName        :: String
    , conclusionName :: String
    , preferName     :: String
    }deriving Show

type KnowledgeSpace = [Knowledge]

type LiteralMap =  Map.HashMap Arg.Name Arg.Literal

-- {-
-- TODOs:
-- Language space in env does not contains neg of rules.
-- -}
-- parseDefaultEnv :: FilePath -> IO (AS ())
-- parseDefaultEnv filePath = do
--     k <- fileToKnowledge filePath
--     prefMap <- fileToPrefMap filePath
--     let
--         l = k2l k
--         r = chainingRule l Arg.neg
--     pure $ mkDefEnv r prefMap

-- -- fileToPrefMap :: FilePath -> IO (L.RdPrefMap, L.KnwlPrefMap)

-- stringToDefEnv :: String -> IO (AS ())
-- stringToDefEnv content = do
--     k <- stringToKnowledge content
--     prefMap <- stringToPrefMap content
--     let
--         l = k2l k
--         r = chainingRule l Default.neg
--     pure $ mkDefEnv r prefMap

getLiteralDict :: AS.AS -> Map.HashMap Arg.Name Arg.Literal 
getLiteralDict as =
    let
        language = Set.toList $ AS.getLanguage . AS.asLiterals $ as
    in Map.fromList $ zip (Arg.getLiteralName <$> language ) language

-- -- parsePreferenceMap :: Env -> L.PreferenceMap
-- -- parsePreferenceMap env =
-- --     let
-- --         rdMap = L.getRdPrefMap . envRdPrefMap $ env
-- --         knMap = L.getKnwlPrefMap . envKnwlPrefMap $ env
-- --     in Map.union rdMap knMap


literalFromSting :: String -> Map.HashMap Arg.Name Arg.Literal-> Arg.Literal 
literalFromSting qName literalDict = fromJust $ Map.lookup qName literalDict 

-- | TODOs:

-- now we have function to generate env from file
-- 1. need function to generation env from elm interface.
-- 2. need to function to union two env and handle possible error, such as conflict of name.
-- 3. Absolutely no error detection and handling functions.

-- | Auxiliary function converting string to data type knowledge

parseDefaultEnv' :: FilePath -> IO AS.AS
parseDefaultEnv' filePath = do
    k <- fileToKnowledge filePath
    prefMap <- fileToPrefMap filePath
    let
        l = k2l k
        liteMap = chainingRule l 
    pure $ mkDefEnv liteMap prefMap

fileToKnowledge :: FilePath -> IO KnowledgeSpace
fileToKnowledge filePath = do
    handle <- openFile filePath  ReadMode
    contents <- hGetContents handle
    pure $ parseWord . removeSpace <$> removeComment (lines contents)

stringToKnowledge :: String -> IO KnowledgeSpace
stringToKnowledge content =  pure $ parseWord . removeSpace <$> removeComment (lines content)

parseWord :: String -> Knowledge
parseWord w =
    let
        [ruleName,ruleBody] = splitOn ":" w
        impName
            | '-' `elem` ruleBody = "->"
            | otherwise = "=>"
        [premies,conC] = splitOn impName ruleBody
        premisesName = splitOn "," premies
        [conclusionName,preferName] = splitOn "," conC
    in Knowledge ruleName premisesName impName conclusionName preferName


k2l :: KnowledgeSpace -> LiteralMap
k2l knowledges = constructLS knowledges Map.empty
    where
        constructLS (k:ks) lsAcc =
            let
                concName = conclusionName k
                priNames = premisesName k
                rName = ruleName k
                iName = impName k
                (updateAtomAcc,primLiterals,concLiteral) = insertAtomsToLanguageSpace concName  priNames lsAcc
                updateRuleAcc = insertRuleToLanguageSpace rName iName primLiterals concLiteral updateAtomAcc
            in constructLS ks updateRuleAcc
        constructLS [] lsAcc  = lsAcc
        insertAtomsToLanguageSpace :: String -> [String] -> LiteralMap -> (LiteralMap, Arg.Rules, Arg.Literal)
        insertAtomsToLanguageSpace concName priNames ls =
            let
                (accPrim, primLiterals) = foldr insertOneAtom (ls,[]) priNames
                (accConc, concLiterals) = insertOneAtom concName (accPrim,[])
            in (accConc, primLiterals, head concLiterals)
            where insertOneAtom n (ll,lbs) =
                            case Map.lookup n ll of
                                Just b -> (ll, b:lbs)
                                Nothing ->
                                    let newl = Arg.Atom n
                                    in (Map.insert n newl ll, newl:lbs)
        insertRuleToLanguageSpace
            :: String
            -> String
            -> Arg.Rules
            -> Arg.Literal
            -> LiteralMap
            -> LiteralMap
        insertRuleToLanguageSpace ruleName imp primies conclusion lspace =
            let
                impSym = if imp == "->" then Arg.S else Arg.D
                bodies = if head primies == Arg.Atom "" then [] else primies
                ruleLiteral = Arg.Rule ruleName bodies impSym conclusion 0
            in Map.insert ruleName ruleLiteral lspace

chainingRule :: LiteralMap -> LiteralMap
chainingRule knowledgeMap =
    let
        ruleList = [ km | km <- Map.toList knowledgeMap , (Arg.getLiteralImp . snd) km == Arg.D || (Arg.getLiteralImp. snd) km == Arg.S]
        ruleMap = Map.fromList [ km | km <- Map.toList knowledgeMap , (Arg.getLiteralImp . snd) km == Arg.D || (Arg.getLiteralImp . snd) km == Arg.S]
    in Map.fromList $ chaining ruleMap <$> ruleList
    where
        chaining rm tp =
            let
                key = fst tp
                p = snd tp
                name = Arg.getLiteralName p
                imp = Arg.getLiteralImp p
                body = Arg.getLiteralBody p
                conC = Arg.getLiteralConc p
                newBody = searchRules rm <$> body
                newConc = searchRules rm conC
            in (key, Arg.Rule name newBody imp newConc 0)
        searchRules :: LiteralMap ->  Arg.Literal -> Arg.Literal
        searchRules rm l =
            case l of
                Arg.Rule {} -> l
                Arg.Atom ""  -> l
                Arg.Atom _  ->
                    let
                        name = Arg.getLiteralName l
                    in
                        if head name == '!'
                            then
                                let
                                    oName = tail name
                                    rO = Map.lookup oName rm
                                in maybe l Conf.neg rO 
                            else
                                let r = Map.lookup name rm
                                in fromMaybe l r

-- -- |
-- -- 1. preference map of rules and premises are separated.
-- -- 2. no record of strict rules because this is not part of any preference set selection methods (weakest-link or last-link).
-- -- TODOs:
-- -- improve the separation method of rules and premises.

fileToPrefMap :: FilePath -> IO AS.PreferenceMap
fileToPrefMap filePath = do
    handle <- openFile filePath  ReadMode
    contents <- hGetContents handle
    stringToPrefMap contents

stringToPrefMap :: String -> IO AS.PreferenceMap
stringToPrefMap contents = do
    let
        records = removeSpace <$> removeComment( lines contents )
        -- premisesLines = [r | r <- records,(":=" `isInfixOf` pack r) || (":-" `isInfixOf` pack r)]
        premisesLines = [r | r <- records, ":=" `isInfixOf` pack r]
        rulesLines = [r | r <- records, r `notElem` premisesLines && not ("->" `isInfixOf` pack r)]
        rdMap =  Map.fromList $ parsePre <$> rulesLines
        kwMap =  Map.fromList $ parsePre <$> premisesLines
        prefMap = Map.union rdMap kwMap
    pure prefMap
  where
      parsePre :: String -> (Arg.Name,Int)
      parsePre s =
          let
              name = head $ splitOn ":" s
              pre = read . last $ splitOn "," s
          in (name,pre)

-- stringToPrefMap :: String -> IO (L.RdPrefMap, L.KnwlPrefMap)
-- stringToPrefMap contents = do
--     let
--         records = removeComment( lines contents )
--         premisesLines = [r | r <- records,(":=" `isInfixOf` pack r) || (":-" `isInfixOf` pack r)]
--         rulesLines = [r | r <- records, r `notElem` premisesLines && not ("->" `isInfixOf` pack r)]
--         rdMap = L.RdPrefMap $ Map.fromList $ parsePre <$> rulesLines
--         kwMap = L.KnwlPrefMap $ Map.fromList $ parsePre <$> premisesLines
--     pure (rdMap,kwMap)
--   where
--       parsePre :: String -> (M.Name,Int)
--       parsePre s =
--           let
--               name = head $ splitOn ":" s
--               pre = read . last $ splitOn "," s
--           in (name,pre)

-- | Remove
-- 1. # : comment line
-- 2. ' ' : lines with empty char
-- 3. "" : lines with no char
removeComment :: [String] -> [String]
removeComment sl = [s | s<-sl ,'#' `notElem` s , s /= ""]

removeSpace :: String -> String 
removeSpace = filter (/= ' ')
-- removeComment sl = [s | s<-sl ,'#' `notElem` s && ' ' `notElem` s, s /= ""]

mkDefEnv :: LiteralMap  -> AS.PreferenceMap -> AS.AS 
mkDefEnv lm pm =
    let
        rules = snd <$> Map.toList lm
        atoms = rmdups $ Arg.getLiteralConc  <$> rules 
        rankRules = do 
            r <- rules 
            pure $ case r of 
                Arg.Rule n b i h _ -> 
                    let rank = Map.lookupDefault 0 (Arg.getLiteralName r) pm 
                    in Arg.Rule n b i h rank 
                a@(Arg.Atom _) -> a

        languages = Set.fromList atoms
        ruleDict = toDict rankRules
    in AS.AS (AS.DefeasibleTheory ruleDict) (AS.L languages) pm Ord.Eli 
    where 
        toDict :: Arg.Rules -> Map.HashMap Arg.Literal [Arg.Literal]
        toDict rules = insertRules rules Map.empty 

        insertRules :: Arg.Rules -> Map.HashMap Arg.Literal [Arg.Literal] -> Map.HashMap Arg.Literal [Arg.Literal] 
        insertRules [] dict = dict 
        insertRules (x:xs) dict = 
            let key = Arg.getLiteralConc x
                rules' = Map.lookupDefault [] key dict 
                newRules = x : rules' 
                accDict = Map.insert key newRules dict
            in insertRules xs accDict
