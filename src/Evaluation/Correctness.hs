{-# LANGUAGE FlexibleContexts #-}
module Evaluation.Correctness where


import           Control.Monad.IO.Class          (MonadIO)
import           Control.Monad.Reader            (MonadReader)

import                  Data.HashMap.Strict (empty)
import qualified           Definitions.Argument as Arg 
import qualified           Definitions.ArgumentationSystem as AS 
import qualified           Definitions.Ordering as Ord 
import qualified           Algorithm.Default as Default 

import  qualified Parser.FileParser as Parser 
import Definitions.Enrichment (Enrichment, LinkFunction, enrich)
import Parser.FileParser (parseDefaultEnv')
import Run.Helper (ghciRunner)
import Run.Env (App, runApp)
import qualified Definitions.Enrichment as Enrich
import Algorithm.Default (weakestLink)

{-correctness test file paths-}
testCdir :: String
testCdir = "data/correctness/"

testPdir :: String
testPdir = "data/performance/"

ctest1 :: String 
ctest1 = testCdir ++ "c-test1.txt" 

ctest2 :: String 
ctest2 = testCdir ++ "c-test2.txt" 

ctest3 :: String 
ctest3 = testCdir ++ "c-test3.txt" 

ctest4 :: String 
ctest4 = testCdir ++ "c-test4.txt" 

ctest5 :: String 
ctest5 = testCdir ++ "c-test5.txt" 

ctest6 :: String 
ctest6 = testCdir ++ "c-test6.txt" 

ctest8 :: String 
ctest8 = testCdir ++ "c-test8.txt" 

corTestAs :: FilePath -> IO AS.AS
corTestAs = parseDefaultEnv' 

{- Test 6 and test 7 
as <- corTestAs ctest6
runner = ghciRunner as 
e = getInitEnrichment as "a"
e1 <- runner $ enrichNaive e
e2 <- runner $ enrichSetNaive e1
e3 <- runner $ enrichSetNaive e2
e4 <- runner $ enrichSetNaive e3
e5 <- runner $ enrichSetNaive e4
a5 <- runner $ attackers $ head e5
runner $ isDefeat lastLink  a5
runner $ isDefeat weakestLink  a5
-}

{- Test 8
as <- corTestAs ctest8
runner = ghciRunner as 
e = getInitEnrichment as "a"
e1 <- runner $ enrichLast e
e2 <- runner $ enrichSetLast e1
e3 <- runner $ enrichSetLast e2
e4 <- runner $ enrichSetLast e3
e5 <- runner $ enrichSetLast e4
e6 <- runner $ enrichSetLast e5
e7 <- runner $ enrichSetLast e6
e8 <- runner $ enrichSetLast e7
let e4' = restoreEnrichability <$> e4
e5' <- runner $ enrichSetNaive e4'
e6' <- runner $ enrichSetNaive e5'
e7' <- runner $ enrichSetNaive e6'
e8' <- runner $ enrichSetNaive e7'
-}


testC :: String -> String -> LinkFunction -> IO (App [Enrichment] -> IO [Enrichment] , [Enrichment])
testC fileName query link = do 
    let 
        filePath = testCdir ++ fileName 
    as <- parseDefaultEnv' filePath 
    let 
        runner = ghciRunner as 
        q = getInitEnrichment as query 
    r <- runner $  enrich link q
    pure (runner , r) 

testlink :: String -> String -> Enrich.LinkFunction -> IO () 
testlink path query link= do 
    (runner ,r1) <- testC path query link 
    print r1 
    r2 <- runner $ Enrich.enrichSet link r1 
    print r2 
    r3 <- runner $ Enrich.enrichSet link r2 
    print r3 
    r4 <- runner $ Enrich.enrichSet link r3 
    print r4 
    r5 <- runner $ Enrich.enrichSet link r4 
    print r5 


{-Test input from file-}

getInitArgument :: AS.AS -> String -> Arg.Argument 
getInitArgument as s = 
    let 
        literalDict = Parser.getLiteralDict as 
        literal = Parser.literalFromSting s literalDict
    in Arg.Argument [] [literal] literal

getInitEnrichment :: AS.AS -> String -> Enrich.Enrichment 
getInitEnrichment as s = 
    let initArg = getInitArgument as s 
    in Enrich.Enrichment mempty (Arg.aLiterals initArg) initArg Enrich.Continue 

{- Test Conflict -}
-- a,b,c,d,e,f,g,h :: Arg.Literal
-- (a,b,c,d,e,f,g,h) = (Arg.Atom "a", Arg.Atom "b" , Arg.Atom "c", Arg.Atom "d",Arg.Atom "e",Arg.Atom "f",Arg.Atom "g",Arg.Atom "h")
-- 
-- r1, r2, r3, r4, r5, r6, r7, r8, r9 :: Arg.Literal 
-- (r1, r2, r3, r4, r5, r6, r7, r8, r9) =
--     ( Arg.Rule "r1" [a] Arg.S b 0
--     , Arg.Rule "r2" [b] Arg.S a 0
--     , Arg.Rule "r3" [c] Arg.D a 0
--     , Arg.Rule "r4" [b] Arg.S e 0
--     , Arg.Rule "r5" [f] Arg.S b 0
--     , Arg.Rule "r6" [g] Arg.D f 0
--     , Arg.Rule "r7" [h] Arg.D f 0
--     , Arg.Rule "r8" [] Arg.S g 0
--     , Arg.Rule "r9" [] Arg.D h 0
--     )
-- 
-- a1 :: Arg.Argument
-- a1 = Arg.Argument [r1,r2,r3] [c,d,e] a 

{- Test Ordering Principle -}

a,b,c,d,e,f,g,h :: Arg.Literal
(a,b,c,d,e,f,g,h) = (Arg.Atom "a", Arg.Atom "b" , Arg.Atom "c", Arg.Atom "d",Arg.Atom "e",Arg.Atom "f",Arg.Atom "g",Arg.Atom "h")

r1, r2, r3, r4, r5, r6, r7, r8, r9 :: Arg.Literal 
(r1, r2, r3, r4, r5, r6, r7, r8, r9) =
    ( Arg.Rule "r1" [a] Arg.S b 3
    , Arg.Rule "r2" [b] Arg.S a 3
    , Arg.Rule "r3" [c] Arg.D a 3
    , Arg.Rule "r4" [b] Arg.S e 3
    , Arg.Rule "r5" [f] Arg.S b 3

    , Arg.Rule "r6" [g] Arg.D f 2
    , Arg.Rule "r7" [h] Arg.D f 3
    , Arg.Rule "r8" [] Arg.S g 2
    , Arg.Rule "r9" [] Arg.D h 2
    )
info1 :: Arg.Info 
info1 = [r1,r2,r3,r4,r5]

info2 :: Arg.Info 
info2 = [r6,r7,r8,r9]
