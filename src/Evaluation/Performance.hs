module Evaluation.Performance where 

import System.CPUTime
import System.Directory (listDirectory)

import Algorithm.Default ( lastLink, weakestLink )
import Definitions.Warranted ( isJustified )
import Run.Helper (ghciRunner)
import Evaluation.Correctness ( corTestAs, getInitEnrichment )
import Hedgehog.Internal.Queue (runTasks)
import Data.List (isInfixOf)
import qualified Definitions.Justification as Justify 


dataPath :: FilePath 
dataPath = "./data/performance/"

getTestList :: IO [FilePath]
getTestList =  do
    fs <- listDirectory dataPath
    let fs' = filter (\f -> not ("test" `isInfixOf` f)) fs 
    pure $ (dataPath <>)  <$> fs' 

saveResult :: FilePath  -> String -> IO () 
saveResult name rs = do 
    let filename = dataPath ++ name  ++ ".txt"
    writeFile filename rs

testEnrichment :: Int -> IO () 
testEnrichment repeat = do 
    testFiles <- getTestList 
    rs <- mapM (repeatEnrichmentTest repeat) testFiles
    mapM_ putStrLn $ show <$> rs 
    print testFiles 
    saveResult "testEnrichment" $ show rs 



testForward :: Int -> IO () 
testForward repeat = do 
    testFiles <- getTestList 
    rs <- mapM (repeatForwardTest repeat) testFiles
    mapM_ putStrLn $ show <$> rs 
    print testFiles 
    saveResult "testForward" $ show rs 

{-enrichment -}
repeatEnrichmentTest :: Int -> FilePath -> IO (FilePath, [Double])
repeatEnrichmentTest n fp = do 
    ts <- traverse n fp []
    pure (fp,ts)
    where 
        traverse :: Int -> FilePath -> [Double] -> IO [Double]
        traverse 0 _ ts = pure ts 
        traverse n f ts = do 
            t <- runEnrichmentTest f 
            traverse (n-1) f (t : ts) 

runEnrichmentTest ::  FilePath -> IO Double 
runEnrichmentTest dataFile = do 
    as <- corTestAs dataFile
    let 
        runner = ghciRunner as 
        e = getInitEnrichment as "a"
    start <- getCPUTime 
    r <- runner $ isJustified lastLink e 
    end <- getCPUTime 
    let diff = fromIntegral (end - start) / (10^12)
    pure diff 

{-forward-}
repeatForwardTest :: Int -> FilePath -> IO (FilePath, [Double])
repeatForwardTest n fp = do 
    print fp 
    ts <- traverse n fp []
    pure (fp,ts)
    where 
        traverse :: Int -> FilePath -> [Double] -> IO [Double]
        traverse 0 _ ts = pure ts 
        traverse n f ts = do 
            t <- runForwardTest f 
            traverse (n-1) f (t : ts) 

runForwardTest ::  FilePath -> IO Double 
runForwardTest dataFile = do 
    as <- corTestAs dataFile
    let 
        runner = ghciRunner as 
        e = getInitEnrichment as "a"
    start <- getCPUTime 
    r <- runner $ isJustified weakestLink e 
    end <- getCPUTime 
    let diff = fromIntegral (end - start) / (10^12)
    pure diff 

{-Defeated Justification -}
testEnrichment' :: Int -> IO () 
testEnrichment' repeat = do 
    testFiles <- getTestList 
    rs <- mapM (repeatEnrichmentTest' repeat) testFiles
    mapM_ putStrLn $ show <$> rs 
    print testFiles 
    saveResult "testEnrichment-d" $ show rs 

testForward' :: Int -> IO () 
testForward' repeat = do 
    testFiles <- getTestList 
    rs <- mapM (repeatForwardTest' repeat) testFiles
    mapM_ putStrLn $ show <$> rs 
    print testFiles 
    saveResult "testForward-d" $ show rs 

repeatEnrichmentTest' :: Int -> FilePath -> IO (FilePath, [Double])
repeatEnrichmentTest' n fp = do 
    ts <- traverse n fp []
    pure (fp,ts)
    where 
        traverse :: Int -> FilePath -> [Double] -> IO [Double]
        traverse 0 _ ts = pure ts 
        traverse n f ts = do 
            t <- runEnrichmentTest' f 
            traverse (n-1) f (t : ts) 

runEnrichmentTest' ::  FilePath -> IO Double 
runEnrichmentTest' dataFile = do 
    as <- corTestAs dataFile
    let 
        runner = ghciRunner as 
        e = getInitEnrichment as "a"
    start <- getCPUTime 
    r <- runner $ Justify.isJustified lastLink e 
    end <- getCPUTime 
    let diff = fromIntegral (end - start) / (10^12)
    pure diff 

repeatForwardTest' :: Int -> FilePath -> IO (FilePath, [Double])
repeatForwardTest' n fp = do 
    print fp 
    ts <- traverse n fp []
    pure (fp,ts)
    where 
        traverse :: Int -> FilePath -> [Double] -> IO [Double]
        traverse 0 _ ts = pure ts 
        traverse n f ts = do 
            t <- runForwardTest' f 
            traverse (n-1) f (t : ts) 

runForwardTest' ::  FilePath -> IO Double 
runForwardTest' dataFile = do 
    as <- corTestAs dataFile
    let 
        runner = ghciRunner as 
        e = getInitEnrichment as "a"
    start <- getCPUTime 
    r <- runner $ Justify.isJustified weakestLink e 
    end <- getCPUTime 
    let diff = fromIntegral (end - start) / (10^12)
    pure diff

coreForwardTest' :: FilePath -> IO Double 
coreForwardTest' file = do 
    as <- corTestAs file  
    let 
        runner = ghciRunner as 
        e = getInitEnrichment as "a"
    start <- getCPUTime 
    r <- runner $ Justify.isJustified weakestLink e 
    end <- getCPUTime 
    let diff = fromIntegral (end - start) / (10^12)
    pure diff 

coreEnrichmentTest' :: FilePath -> IO Double 
coreEnrichmentTest' dataFile = do 
    as <- corTestAs dataFile
    let 
        runner = ghciRunner as 
        e = getInitEnrichment as "a"
    start <- getCPUTime 
    r <- runner $ Justify.isJustified lastLink e 
    end <- getCPUTime 
    let diff = fromIntegral (end - start) / (10^12)
    pure diff 