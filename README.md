# naive-backchaining

## 1.Correctness Check 
Following examples return results according to the argument structures as shown in charts. 

- Example 1: Enrichment Test, use naive link function, circular should be detected. 
    
    ```
    ctest1 = testCdir ++ "c-test1.txt"
    as <- corTestAs ctest1
    runner = ghciRunner as 
    e = getInitEnrichment as "a"
    e1 <- runner $ enrichNaive e
    e2 <- runner $ enrichSetNaive e1
    e3 <- runner $ enrichSetNaive e2
    e4 <- runner $ enrichSetNaive e3
    e3 == e4
    isComplete <$> e3
    e5 <- runner $ enrichSetNaive e4
    e4 == e5 
    isComplete <$> e4
    ```
    ![test-1](imgs/c-test1.png)

- Example 2: Enrichment Test, use naive link function, circle should be detected. 
    ```
    ctest2 = testCdir ++ "c-test2.txt" 
    as <- corTestAs ctest2
    runner = ghciRunner as 
    e = getInitEnrichment as "a"
    e1 <- runner $ enrichNaive e
    e2 <- runner $ enrichSetNaive e1
    e3 <- runner $ enrichSetNaive e2
    e4 <- runner $ enrichSetNaive e3
    e5 <- runner $ enrichSetNaive e4
    e4 == e5 
    isComplete <$> e4
    ```
    ![test-2](imgs/c-test2.png)

- Example 3: Link function test, use weakest link and last link, enrichment process should reach fix-point when necessary information has be collected. 

    ```
    ctest3 = testCdir ++ "c-test3.txt"
    as <- corTestAs ctest3 
    runner = ghciRunner as 
    e = getInitEnrichment as "a"
    e1 <- runner $ enrichNaive e
    e2 <- runner $ enrichSetNaive e1
    e3 <- runner $ enrichSetNaive e2
    e4 <- runner $ enrichSetNaive e3
    e5 <- runner $ enrichSetNaive e4

    e1' <- runner $ enrichLast e
    e2' <- runner $ enrichSetLast e1'
    e3' <- runner $ enrichSetLast e2'
    e4' <- runner $ enrichSetLast e3'
    e5' <- runner $ enrichSetLast e4'

    e3' == e4'                          -- e3' is complete thus e3' == e4' is True.
    e3 == e4                            -- e3 is not complete thus e3 /= e4 .

    e1'' <- runner $ enrichWeakest e
    e2'' <- runner $ enrichSetWeakest e1''
    e3'' <- runner $ enrichSetWeakest e2''
    e4'' <- runner $ enrichSetWeakest e3''
    e5'' <- runner $ enrichSetWeakest e4''

    restoreEnrichability <$> e3'
    ```
    ![test-3](imgs/c-test3.png)

- Example 4: Attack test. 
    - attackers' returns attack positions of most recent included evidence. 
    - attackers'' returns all attack positions 
    ```
    ctest4 = testCdir ++ "c-test4.txt"
    as <- corTestAs ctest4 
    runner = ghciRunner as 
    e = getInitEnrichment as "a"
    e1 <- runner $ enrichNaive e
    e2 <- runner $ enrichSetNaive e1
    e3 <- runner $ enrichSetNaive e2
    e4 <- runner $ enrichSetNaive e3
    e5 <- runner $ enrichSetNaive e4

    att' <- runner $ attackers' e
    att1' <- runner $ mapM attackers' e1
    att2' <- runner $ mapM attackers' e2
    att3' <- runner $ mapM attackers' e3
    att4' <- runner $ mapM attackers' e4
    att5' <- runner $ mapM attackers' e5

    att'' <- runner $ attackers'' e
    att1'' <- runner $ mapM attackers'' e1
    att2'' <- runner $ mapM attackers'' e2
    att3'' <- runner $ mapM attackers'' e3
    att4'' <- runner $ mapM attackers'' e4
    att5'' <- runner $ mapM attackers'' e5
    ```
    ![test-4](imgs/c-test4.png)


- Example 4': Naive Justified test 
```
ctest4' = testCdir ++ "c-test4.txt"
as <- corTestAs ctest4'
runner = ghciRunner as 
e = getInitEnrichment as "a"

r <- runner $ isJustified lastLink e 
```

- Example 5: 
```
ctest5 = testCdir ++ "c-test5.txt"
as <- corTestAs ctest5
runner = ghciRunner as 
e = getInitEnrichment as "a"

r <- runner $ isJustified lastLink e 
```
- Example 6: 
```
ptest1 = testPdir ++ "4.txt"
as <- corTestAs ptest1
runner = ghciRunner as 
e = getInitEnrichment as "a"

r <- runner $ isJustified lastLink e 
r' <- runner $ isJustified weakestLink e 
```
- Example 7: 
