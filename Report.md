
#### Definitions & Implementation 
1. Argument 
    ```
    data Argument = Argument {aPath :: Path, aLiterals :: [Literal], aConc :: Literal } 
    ```
1. Enrichment 
    ```
    data Enrichment = Enrichment
    { getEnrichmentLog  :: EnrichLog
    , getEnrichmentInfo :: Arg.Info
    , getEnrichmentArg  :: Arg.Argument
    } 
    ```
    - `EnrichLog` : $\displaystyle\bigcup_1^i \bar{R}(enrich \circ \overleftarrow{enrich}^i A)$. All evidence (rules) being used in `i` steps enrichment and related siblings, e.g '2' and '3' are sibling enrichment.  
    ![Correctness test 1](./imgs/c-test1.png)
    
    - `Info` : Interested information . 
1.  **link function** (***Weakest-link and Last-Link***)
    ```
    type LinkFunction = Arg.Info -> Arg.Argument -> Support -> (Arg.Info, Arg.Argument)
    ```
    Given an `Argument`, `support` evidence of this argument and `Info` collected in previous enrichment process, return interested information in this enrichment step and the enriched argument. 

1. `enrich` is a combinator of two functions: 
    1. `enrichability` : Enrichability test and returning support evidence if enrichable. 
    1. `link` : instance of link function . 

1. Argumentation System (AS)
    ```
    data AS = AS
    { asRules          :: DefeasibleTheory
    , asLiterals       :: L
    , asOrderPrinciple :: Ord.Principle
    }
    ```
    Given a knowledge base, there is a corresponding argumentation system contains `Defeasible Theory` and `L`(logical language). We also need to pick an `Ordering Principle` (Eli or Dem). 

1. Preference Ordering : Given a principle (`Eli` or `Dem`), compare the preference of two `Info`s.
    ```
    (>:) :: Principle -> Arg.Info -> Arg.Info -> Bool 
    (>:) _ _ [] = False  
    (>:) _ [] (_:_) = True 
    (>:) Eli as bs = 
        let p = map (\x -> all ( x <=:) as) bs 
        in or p 
    (>:) Dem as bs = 
        let p =map (\x -> all ( x >=: ) bs) as 
        in or p 
    ```     

1. Attacks 
    Given an `Enrichment` and relies on the `L`(logic language) in the argumentation system, we could tell if there is any attacks on this enrichment and the type of the attack. 
    ```
    attackers ::
    ( AS.Has AS.L env
    , MonadIO m
    , MonadReader env m
    ) => Enrich.Enrichment -> m Attacks
    ```
    ```
    data Rebutter = Rebutter Arg.Argument Arg.Literal 
    type Rebutters = [Rebutter]
    data Undercutter = Undercutter Arg.Argument Arg.Literal 
    type Undercutters = [Undercutter]
    data Attacks where
        Rebut :: Rebutters -> Attacks
        Undercut :: Undercutters -> Attacks
        Both ::  Rebutters -> Undercutters -> Attacks
        Peace :: Attacks
    ```
    Different instance carries different computational semantics: 
    1. check attack status in after every enrichment process, indicates we only need to check the most included evidence. (current choice)
    2. check attack status after some enrichment steps. (future choice). 

1. Defeat 
    To compute if a given attack is successful 
    ```
    isDefeat ::
    ( AS.Has AS.DefeasibleTheory env
    , AS.Has Ord.Principle env
    , MonadIO m
    , MonadReader env m
    )
    => Enrich.LinkFunction
    -> Attacks.Attacks
    -> m [Outcome]
    ```
    - Relies on `link function` and `Defeasible Theory` in the AS to collect necessary information and relies on `Ord.Principle` to compute the ordering preference. 
    ```
    data Conflict = Conflict {attacker :: Enrich.Enrichment, defender :: Enrich.Enrichment} 
    data Outcome = Outcome {success :: [Conflict], failure :: [Conflict]}
    ```
    **Problem:**
    1. `last-link` implies the type `Outcome` makes no sense, there will always be one instance of `Conflict`.
    1. `weakest-link` implies the whole idea of `enriching and testing step by step` makes no sense, because it require the fully construction of the entire argument. Actually, only a small proportion of cases requires partial construction of the argument.
    1. `Naive-link` complete may include unnecessary support in the case that the argument is indeed defeated at proposition $\alpha$, it is also necessary to use `naive-link` to enrich the argument in the case that the argument is not defeated (warranted). 
       

####Evaluation
1. Two advantages of backward searching 
    1. Construct argument backward step by step . 
        So that only relied argument being computed. 
    1. link function that controls the backward construction. 
        So that only necessary information being collected. (**Last-link**)

    Benchmark should be designed focus on these two aspects. 
1. Benchmark design : 
    - knowledge base / argumentation system which contains many disconnected sub-graph(argument as vertex, attacks as edge). 
    Focus on evaluation advantage 1. 
    - Warranted and Unwarranted 
    ![Correctness test 1](./imgs/warranted-unwarranted.png)
        - a warranted proposition indicates : 
            1. at least one of corresponding enrichment must be fully constructed. 
            1. branch which root is warranted enrichment must satisfy: 
                1. all leaves must be on even number of layers. 
                1. all odd number node does not need to be fully constructed. 
                1. at least one enrichment of even number layer proposition is self warranted. 
    
            To increase the number of enrichment that is half purple and half blue (as shown in the graph) and compare with implementation of standard ASPIC (forward chaining) definition. Focus on evaluate advantage 2. 

**TODO** : 

1. Implementation similar algorithm based on standard definition. 
1. Generate two types of benchmark and compare backward search algorithm with algorithm based on standard definition.
1. Design some link between last-link and weakest-link that makes the definition of conflict and the idea of enrichment both being useful (Attacks.semantics_2, There are options to take. when the check the attack). 
1. Record explored argument space and compare with the entire argument space in a given knowledge base. 
1. Remove unnecessary overhead design, if necessary discuss what purpose does this abstraction server. 

#### Future works. 
1. Way to control how argumentation space (knowledge base) is generated.
1. How to introduce heuristic information (prior knowledge) to guide search strategy .
1. Could it be possible to give an abstract definition of search strategy (just like the link function) ? How it cooperate with heuristic information? 


**Version 1 benchmark design**
- There were four main parameters being used to control the benchmark construction. 
1. Split factor: How rules in the knowledge base are related to each other. 
1. Total Depth of an argument: the length from conclusion to the axiom / premises. 
1. Defeasible Depth of an argument: length from conclusion to the last defeasible rules. 
1. Length of the defeasible Chain: The length from query argument to a selfwarranted argument . 

- Problem : 
1. The size of an knowledge base grows too fast, given that split factor is 2, defeasible depth is 4, there are 128+256 possible vulnerable points in the fist level of a defeasible chain. 
2. Some test case is beyond current consideration. For example, a selfwarranted argument with total length of 2 and another argument with total length of 10 and the length tof defeasible chain is 200 (tumor path). With no prior knowledge (heuristic information) it is hard to deal with such case. 

**Version 2 benchmark design** 
The target of backward searching algorithm and implementation is to try to avoid unnecessary computation. So the defeasible depth can be ignored at this stage and it is acceptable to use a fixed split factor or hand engineering argument structure. 
There will be three main parameters: 
1. Total Depth of an argument: the length from conclusion to the axiom / premises. 
1. Defeasible Depth of an argument: length from conclusion to the last defeasible rules. 
1. Number of confusion argument : arguments that is not a valid justification of the query. 

There are four types of basic test structures. 
- Type-0 : a self-warranted argument with an unsuccessfully attacker. **+** n confusion argument.
- Type-1 : a warranted argument that is rebutted by an unwarranted defeater (defeated by a selfwarranted argument), the length of the defeated chain is 3. **+** n confusion argument.
- Type-2 : a warranted argument that is undercut by unwarranted defeater (defeated by a selfwarranted argument), the length of the defeated chain is 3. **+** n confusion argument.
- Type-3 : a warranted argument that is rebutted and undercut by different unwarranted defeaters (defeated by a selfwarranted argument), the length of the defeated chain is 3. **+** n confusion argument.

- Performance Chart 
```
    total_depths = [10,12,14,16,18,20]
    def_depth_factors = [0.2,0.3,0.4,0.5,0.6,0.7,0.8]
    computsion_num_list = [2,5]
    top_prop_num = 50
```
**type 0**
![type 0](./imgs/type-0.png)
**type 1**
![type 1](./imgs/type-1.png)
**type 2**
![type 2](./imgs/type-2.png)
**type 3**
![type 3](./imgs/type-3.png)


1. Terms issue : The argument draws one conclusion (as in standard ASPIC) would increase implementation difficulty (? better description), the concept of argument that draw many conclusion (path / enrichment) would improve the modularity of the algorithm. 

![Type Relations](./imgs/type-relations.png)

####Performance Evaluations

depth_first vs width_first 

####Correctness Test Cases                    
- Test 1: file 'data/correctness/c-test1.txt'
    - test pending argument. 
    - test parent siblings effects.
![Correctness test 1](./imgs/c-test1.png)

- Test 2: file 'data/correctness/c-test2.txt'
    - test the scope of parent sibling effect. 
    - test generating equal final paths. 
![Correctness test 1](./imgs/c-test2.png)

- Test 3: file 'data/correctness/c-test3.txt'
    - test the effects of different link functions.
![Correctness test 1](./imgs/c-test3.png)

- Test 4: file 'data/correctness/c-test3.txt'
    - test conflict definition
![Correctness test 1](./imgs/c-test4.png)
```
as <- parseDefaultEnv' ctest4
runner =ghciRunner as
r1 = getInitEnrichment as "a"
r2 <- runner $ enrich weakestLink r1
r2
[< EnrichLog : [( r1: b c->a, 0 )]
 , EnrichInfo: []
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )]], Literals : [b,c] }>
]
runner $ attackers $ head r2
Peace
r3 <- runner $ enrichSet weakestLink r2
r4 <- runner $ enrichSet weakestLink r3
r4
[< EnrichLog : [( r1: b c->a, 0 ),( r3: e->b, 0 ),( r2: d->b, 0 ),( r4: f->c, 0 ),( r6: h=>e, 1 ),( r7: i->f, 0 )]
 , EnrichInfo: [( r6: h=>e, 1 )]
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )],[( r3: e->b, 0 ),( r4: f->c, 0 )],[( r6: h=>e, 1 ),( r7: i->f, 0 )]], Literals : [h,i] }>
,< EnrichLog : [( r1: b c->a, 0 ),( r3: e->b, 0 ),( r2: d->b, 0 ),( r4: f->c, 0 ),( r5: g=>d, 1 ),( r7: i->f, 0 )]
 , EnrichInfo: [( r5: g=>d, 1 )]
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )],[( r2: d->b, 0 ),( r4: f->c, 0 )],[( r5: g=>d, 1 ),( r7: i->f, 0 )]], Literals : [g,i] }>
]
runner $ attackers $ head r4
Peace
runner $ attackers $ last r4
Rebut (Rebutters [!d])
r5 <- runner $ enrichSet weakestLink r4
r5' <- runner $ enrichSet lastLink r4
r5
[< EnrichLog : [( r1: b c->a, 0 ),( r3: e->b, 0 ),( r2: d->b, 0 ),( r4: f->c, 0 ),( r6: h=>e, 1 ),( r7: i->f, 0 ),( r9: k=>h, 1 ),( r10: l->i, 0 )]
 , EnrichInfo: [( r6: h=>e, 1 ),( r9: k=>h, 1 )]
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )],[( r3: e->b, 0 ),( r4: f->c, 0 )],[( r6: h=>e, 1 ),( r7: i->f, 0 )],[( r9: k=>h, 1 ),( r10: l->i, 0 )]], Literals : [k,l] }>
,< EnrichLog : [( r1: b c->a, 0 ),( r3: e->b, 0 ),( r2: d->b, 0 ),( r4: f->c, 0 ),( r5: g=>d, 1 ),( r7: i->f, 0 ),( r8: j=>g, 1 ),( r10: l->i, 0 )]
 , EnrichInfo: [( r5: g=>d, 1 ),( r8: j=>g, 1 )]
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )],[( r2: d->b, 0 ),( r4: f->c, 0 )],[( r5: g=>d, 1 ),( r7: i->f, 0 )],[( r8: j=>g, 1 ),( r10: l->i, 0 )]], Literals : [j,l] }>
]
runner $ attackers $ last r5
Both (Rebutters [!d]) (Undercutters [( !r8: j=>g, 0 )])
runner $ attackers' $ last r5
Undercut (Undercutters [( !r8: j=>g, 0 )])
r6 <- runner $ enrichSet weakestLink r5
r6
[< EnrichLog : [( r1: b c->a, 0 ),( r3: e->b, 0 ),( r2: d->b, 0 ),( r4: f->c, 0 ),( r6: h=>e, 2 ),( r7: i->f, 0 ),( r9: k=>h, 3 ),( r10: l->i, 0 ),( r12: ->k, 0 ),( r13: =>l, 2 )]
 , EnrichInfo: [( r6: h=>e, 2 ),( r9: k=>h, 3 ),( r13: =>l, 2 )]
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )],[( r3: e->b, 0 ),( r4: f->c, 0 )],[( r6: h=>e, 2 ),( r7: i->f, 0 )],[( r9: k=>h, 3 ),( r10: l->i, 0 )],[( r12: ->k, 0 ),( r13: =>l, 2 )]], Literals : [] }>
,< EnrichLog : [( r1: b c->a, 0 ),( r3: e->b, 0 ),( r2: d->b, 0 ),( r4: f->c, 0 ),( r5: g=>d, 2 ),( r7: i->f, 0 ),( r8: j=>g, 3 ),( r10: l->i, 0 ),( r11: ->j, 0 ),( r13: =>l, 2 )]
 , EnrichInfo: [( r5: g=>d, 2 ),( r8: j=>g, 3 ),( r13: =>l, 2 )]
, Enrichment: { Conclusion : a, path : [[( r1: b c->a, 0 )],[( r2: d->b, 0 ),( r4: f->c, 0 )],[( r5: g=>d, 2 ),( r7: i->f, 0 )],[( r8: j=>g, 3 ),( r10: l->i, 0 )],[( r11: ->j, 0 ),( r13: =>l, 2 )]], Literals : [] }>
]
runner $ attackers $ last r6
Both (Rebutters [!l,!d]) (Undercutters [( !r8: j=>g, 0 )])
runner $ attackers' $ last r6
Rebut (Rebutters [!l])
```
```
r  = getInitEnrichment as "a"
att = getInitEnrichment as "!d"
attr <- runner $ enrichFix lastLink [att]
res <- runner $ enrichFix weakestLink  [r]
(>:) Dem (getEnrichmentInfo (head attr)) (getEnrichmentInfo (head res))
False
res <- runner $ enrichFix lastLink  [r]
(>:) Eli (getEnrichmentInfo (head attr)) (getEnrichmentInfo (head res))
True
```
- Test 5: file 'data/correctness/c-test5.txt'
    - test basic query result.
![Demo Graph succ](./imgs/c-test5.png)

- Test 6: file 'data/correctness/c-test6.txt' 
    - test medium size query knowledge base. 

